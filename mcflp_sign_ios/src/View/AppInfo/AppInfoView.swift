//
//  AppInfoView.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 22..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import UIKit


class AppInfoView: UIViewController {
    override func viewDidLoad() {
        setStatusBar()
        setTitlebar()
        setView()
    }
    
    private func setStatusBar() {
        self.view.frame = ((UIApplication.shared.delegate as! AppDelegate).window?.frame)!
        self.view.backgroundColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = true

        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = HexColor.hexStringToUIColor(hex: "#4380DB")
        statusBarView.backgroundColor = statusBarColor
        self.view.addSubview(statusBarView)
    }
    
    private func setTitlebar() {
        let bar = AppInfoImages.bar
        bar.frame = AppInfoFrames.bar
        self.view.addSubview(bar)
//
        let back = UIButton()
        back.frame = AppInfoFrames.back
        back.setImage(UIImage(named: "icon_menu"), for: .normal)
//        back.setTitleColor(HexColor.hexStringToUIColor(hex: "#FFFFFF"), for: .normal)
//        back.titleLabel?.font = UIFont(name: "NanumGothicBold", size: ScreenInfo.getWidth(width:40))!
        back.addTarget(self, action: #selector(AppInfoView.backButtonClick(_sender:)), for: .touchUpInside)

        self.view.addSubview(back)
//
        let titleLabel = UILabel()
        titleLabel.frame = AppInfoFrames.titleLabel
        titleLabel.text = AppInfoStrings.titleLabel
        titleLabel.textColor = AppInfoColors.titleLabel
        titleLabel.font = AppInfoFonts.titleLabel
        titleLabel.adjustsFontSizeToFitWidth = true

        self.view.addSubview(titleLabel)
    }
    
    private func setView() {
        let icon = UIImageView(image: UIImage(named: "logo2"))
        icon.frame = CGRect(x: (ScreenInfo.currentDeviceWidth / 2) - ScreenInfo.getWidth(width: 35),
                            y: UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 60) + ScreenInfo.getHeight(height: 80),
                            width: ScreenInfo.getWidth(width: 70),
                            height: ScreenInfo.getWidth(width: 70))
        
        self.view.addSubview(icon)
        
        let title = UILabel()
        title.frame = CGRect(x: (ScreenInfo.currentDeviceWidth / 2) - ScreenInfo.getWidth(width: 150),
                             y: UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 60) + ScreenInfo.getHeight(height: 160),
                             width: ScreenInfo.getWidth(width: 300),
                             height: ScreenInfo.getWidth(width: 70))
        title.textAlignment = .center
        title.font = UIFont(name: "NanumGothicBold", size: ScreenInfo.getWidth(width:20))
        title.text = "연명의료 정보처리시스템"
        
        self.view.addSubview(title)
        
        let currentVer : String = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        
        let version = UILabel()
        version.frame = CGRect(x: (ScreenInfo.currentDeviceWidth / 2) - ScreenInfo.getWidth(width: 150),
                             y: UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 60) + ScreenInfo.getHeight(height: 200),
                             width: ScreenInfo.getWidth(width: 300),
                             height: ScreenInfo.getWidth(width: 70))
        version.textAlignment = .center
        version.font = UIFont(name: "NanumGothic", size: ScreenInfo.getWidth(width:18))
        version.text = "버전 \(currentVer)"
        
        self.view.addSubview(version)
        
        let compare = UILabel()
        compare.frame = CGRect(x: (ScreenInfo.currentDeviceWidth / 2) - ScreenInfo.getWidth(width: 150),
                               y: UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 60) + ScreenInfo.getHeight(height: 230  ),
                               width: ScreenInfo.getWidth(width: 300),
                               height: ScreenInfo.getWidth(width: 70))
        compare.textAlignment = .center
        compare.font = UIFont(name: "NanumGothic", size: ScreenInfo.getWidth(width:18))
        
        self.view.addSubview(compare)
        
        if "2.0" == currentVer {
            compare.text = "최신 버전이 아닙니다."
        } else {
            compare.text = "최신 버전입니다."
        }
    }
    
    @objc public func backButtonClick(_sender: UIButton) {
        if CertificationView.slideMenuController.isLeftOpen() {
            CertificationView.slideMenuController.closeLeft()
        } else {
            CertificationView.slideMenuController.openLeft()
        }
//        let mainView = MainView()
//
//        CertificationView.slideMenuController.changeMainViewController(mainView, close: true)
    }
}
