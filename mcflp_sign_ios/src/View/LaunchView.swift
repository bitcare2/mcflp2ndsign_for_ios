//
//  LaunchView.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 11. 23..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import UIKit

class LaunchView: UIViewController {
    private var loginPresenter: LoginPresenter!
    
    var userNameTextField: ImageTextField!
    var pwTextField: ImageTextField!
    
    var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.setStatusBar()
        self.setBackground()

        
        
        //        setView()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func apiAddDeviceInfo() {
        //        let certificationView = CertificationView()
        //        self.present(certificationView, animated: true, completion: nil)
    }
    
    private func setStatusBar() {
        self.view.frame = ((UIApplication.shared.delegate as! AppDelegate).window?.frame)!
        self.view.backgroundColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = true
        
        //        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        //        let statusBarColor = HexColor.hexStringToUIColor(hex: "#4380DB")
        //        statusBarView.backgroundColor = statusBarColor
        //        self.view.addSubview(statusBarView)
    }
    
    private func setBackground() {
        //background
        let bgView = LoginViewImages.bgView
        bgView.frame = LoginViewFrames.bgView
        self.view.addSubview(bgView)
        
        let logo = UIImageView(image: UIImage(named: "logo2"))
        logo.frame = CGRect(x: ScreenInfo.getWidth(width: 130),
                            y: ScreenInfo.getHeight(height: 140),
                            width: 110,
                            height: 110)
        
        self.view.addSubview(logo)
        
        let title1 = UILabel()
        title1.frame = CGRect(x: ScreenInfo.getWidth(width: 88),
                              y: ScreenInfo.getHeight(height: 280),
                              width: ScreenInfo.getWidth(width: 200),
                              height: ScreenInfo.getHeight(height: 50))
        title1.font = UIFont(name: "NanumGothicBold", size: ScreenInfo.getHeight(height: 26))
        title1.textColor = .white
        title1.textAlignment = .center
        title1.text = "연명의료"
        
        self.view.addSubview(title1)
        
        let title2 = UILabel()
        title2.frame = CGRect(x: ScreenInfo.getWidth(width: 88),
                              y: ScreenInfo.getHeight(height: 320),
                              width: ScreenInfo.getWidth(width: 200),
                              height: ScreenInfo.getHeight(height: 50))
        title2.font = UIFont(name: "NanumGothicBold", size: ScreenInfo.getHeight(height: 26))
        title2.textColor = .white
        title2.textAlignment = .center
        title2.text = "정보처리시스템"
        
        self.view.addSubview(title2)
        
        loginButton = UIButton()
        loginButton.frame = CGRect(x: ScreenInfo.getWidth(width: 140),
                                   y: ScreenInfo.getHeight(height: 390),
                                   width: ScreenInfo.getWidth(width: 100),
                                   height: ScreenInfo.getHeight(height: 40))
        loginButton.layer.cornerRadius = 20.0
        loginButton.setTitle("서명전용", for: .normal)
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.backgroundColor = HexColor.hexStringToUIColor(hex: "#4380DB")
        
        self.view.addSubview(loginButton)
        
        //logo
        let bottomLogo = LoginViewImages.logo
        bottomLogo.frame = LoginViewFrames.bottomLogo
        
        self.view.addSubview(bottomLogo)
        
        //logo title
        let logoTitle = UILabel()
        logoTitle.frame = LoginViewFrames.logoTitle
        logoTitle.text = LoginViewStrings.bottom
        logoTitle.textColor = LoginViewColors.logoTitle
        logoTitle.font = LoginViewFonts.LogoTitle
        
        self.view.addSubview(logoTitle)
    }
    
    private func setView() {
        
        let paadingNameView = UIView(frame: CGRect(x: 0,
                                                   y: 0,
                                                   width: ScreenInfo.getWidth(width: 20),
                                                   height: ScreenInfo.getHeight(height: 50)))
        let paadingPasswordView = UIView(frame: CGRect(x: 0,
                                                       y: 0,
                                                       width: ScreenInfo.getWidth(width: 20),
                                                       height: ScreenInfo.getHeight(height: 50)))
        
        let color = UIColor.black
        
        userNameTextField = ImageTextField()
        userNameTextField.frame = CGRect(x: ScreenInfo.getWidth(width: 50),
                                         y: ScreenInfo.getHeight(height: 150),
                                         width: ScreenInfo.getWidth(width: 275),
                                         height: ScreenInfo.getHeight(height: 60))
        userNameTextField.leftViewMode = UITextFieldViewMode.always
        userNameTextField.leftView = paadingNameView
        userNameTextField.layer.borderWidth = 1.0
        userNameTextField.layer.borderColor = color.cgColor
        userNameTextField.layer.cornerRadius = 5.0
        userNameTextField.placeholder = "user name"
        
        self.view.addSubview(userNameTextField)
        
        pwTextField = ImageTextField()
        pwTextField.frame = CGRect(x: 50,
                                   y: 230,
                                   width: 275,
                                   height: 60)
        pwTextField.leftViewMode = UITextFieldViewMode.always
        pwTextField.leftView = paadingPasswordView
        pwTextField.layer.borderWidth = 1.0
        pwTextField.layer.borderColor = color.cgColor
        pwTextField.layer.cornerRadius = 5.0
        pwTextField.placeholder = "password"
        
        self.view.addSubview(pwTextField)
        
        
        
    }
        
        
}
