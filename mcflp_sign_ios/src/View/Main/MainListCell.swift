//
//  MainListCell.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 4..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import UIKit

protocol MainListCellProtocol {
    func onClick(index: Int)
}

class MainListCell: UITableViewCell {
    public var name: UILabel = UILabel()
    public var date: UILabel = UILabel()
    public var type: UILabel = UILabel()
    public var requestName: UILabel = UILabel()
    public var requestStateName: UILabel = UILabel()
    public var number: UILabel = UILabel()
    
    private var mainListHaribo: MainListHaribo!
    private var index: Int = 0
    private var mainListCellProtocol: MainListCellProtocol!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("MainListCell init(coder:)")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        contentView.backgroundColor = HexColor.hexStringToUIColor(hex: "#FAFAFA")
//        contentView.layer.cornerRadius = 5.0
//        contentView.layer.masksToBounds = false
//        contentView.layer.shadowColor = UIColor.black.cgColor
//        contentView.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(3.0))
//        contentView.layer.shadowOpacity = 0.5

    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        contentView.frame = UIEdgeInsetsInsetRect(contentView.frame, UIEdgeInsetsMake(16, 16, 2, 16))
//    }
    
    private func setLabel() {
//        name = UILabel()
        name.frame = MainListCellFrames.name
        name.font = MainListCellFonts.name
        name.textColor = MainListCellColors.name
        name.text = mainListHaribo.getName()
        name.sizeToFit()
        
        contentView.addSubview(name)
        
//        date = UILabel()
        date.frame = CGRect(x: ScreenInfo.getWidth(width: 32) + name.frame.width,
                            y: ScreenInfo.getHeight(height: 1) ,
                            width: ScreenInfo.getWidth(width: 100),
                            height: ScreenInfo.getHeight(height: 50))
        date.font = MainListCellFonts.date
        date.textColor = MainListCellColors.date
//        date.text = "\(mainListHaribo.getName().count)"
        date.text = mainListHaribo.getDate()
        name.sizeToFit()
        
        contentView.addSubview(date)
        
        requestStateName.frame = CGRect(x: ScreenInfo.getWidth(width: 25) + name.frame.width + date.frame.width,
                                        y: ScreenInfo.getHeight(height: 1) ,
                                        width: ScreenInfo.getWidth(width: 100),
                                        height: ScreenInfo.getHeight(height: 50))
        requestStateName.font = MainListCellFonts.requestStateName
        requestStateName.textColor = MainListCellColors.requestStateName
//        requestStateName.text = mainListHaribo.getRequestStateName()
        
        let attributedString = NSMutableAttributedString(string: mainListHaribo.getRequestStateName())
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 20 // Whatever line spacing you want in points
        attributedString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(attributedString.length, 0))
        requestStateName.attributedText = attributedString
        
        contentView.addSubview(requestStateName)
         
        
//        type = UILabel()
        type.frame = MainListCellFrames.type
        type.font = MainListCellFonts.type
        type.textColor = MainListCellColors.type
        type.text = mainListHaribo.getType()
        
        contentView.addSubview(type)
        
//        state = UILabel()
        requestName.font = MainListCellFonts.state
        requestName.textColor = MainListCellColors.state
        requestName.textAlignment = .center
        requestName.text = mainListHaribo.getRequestName()
        requestName.layer.masksToBounds = true
        requestName.layer.cornerRadius = ScreenInfo.getWidth(width: 15)
        
        if mainListHaribo.getRequestName() == "작성자 서명 요청" {
            requestName.frame = MainListCellFrames.requestName1
            requestName.backgroundColor = MainListCellColors.stateBrown
            number.frame = MainListCellFrames.number1
        } else if mainListHaribo.getRequestName() == "환자 서명 요청" {
            requestName.frame = MainListCellFrames.requestName2
            requestName.backgroundColor = MainListCellColors.stateBrown
            number.frame = MainListCellFrames.number2
        } else if mainListHaribo.getRequestName() == "환자 가족 서명 요청" {
            requestName.frame = MainListCellFrames.requestName3
            requestName.backgroundColor = MainListCellColors.stateWhine
            number.frame = MainListCellFrames.number3
        } else if mainListHaribo.getRequestName() == "전문의 협진 요청" {
            requestName.frame = MainListCellFrames.requestName4
            requestName.backgroundColor = MainListCellColors.stateBlue
            number.frame = MainListCellFrames.number4
        } else if mainListHaribo.getRequestName() == "법정대리인 서명 요청" {
            requestName.frame = MainListCellFrames.requestName5
            requestName.backgroundColor = MainListCellColors.stateGreen
            number.frame = MainListCellFrames.number5
        } else if mainListHaribo.getRequestName() == "작성자 확인 서명 요청" {
            requestName.frame = MainListCellFrames.requestName5
            requestName.backgroundColor = MainListCellColors.stateBrown
            number.frame = MainListCellFrames.number5
        } else {
            requestName.frame = MainListCellFrames.requestName5
            number.frame = MainListCellFrames.number5
        }
        
        
        
        contentView.addSubview(requestName)
        
//        number = UILabel()
        number.font = MainListCellFonts.number
        number.textColor = MainListCellColors.number
        number.text = "No:" + mainListHaribo.getNumber()

        contentView.addSubview(number)
        
        
        let click = UIButton()
        click.frame = CGRect(x: 0, y: 0, width: ScreenInfo.currentDeviceWidth, height: ScreenInfo.getHeight(height: 130))
        click.addTarget(self, action: #selector(MainListCell.cellButtonClick(_sender:)), for: .touchUpInside)
        
        contentView.addSubview(click)
        
        let separator = UIView()
        separator.frame = CGRect(x: 0, y: ScreenInfo.getHeight(height: 129), width: ScreenInfo.currentDeviceWidth, height: ScreenInfo.getHeight(height: 1))
        separator.backgroundColor = HexColor.hexStringToUIColor(hex: "#ACACAC")
        
        contentView.addSubview(separator)
    }
    
    public func setData(mainListHaribo: MainListHaribo) {
        self.mainListHaribo = mainListHaribo
        setLabel()
//        self.name.text = mainListHaribo.getName()
//        self.date.text = mainListHaribo.getDate()
//        self.type.text = mainListHaribo.getType()
//        self.state.text = mainListHaribo.getState()
//        self.number.text = mainListHaribo.getNumber()
        
        
    }
    
    public func setIndex(index: Int) {
        self.index = index
        
        if index % 2 == 0 {
            contentView.backgroundColor = HexColor.hexStringToUIColor(hex: "#FAFAFA")
        } else {
            contentView.backgroundColor = HexColor.hexStringToUIColor(hex: "#FFFFFF")
        }
    }
    
    public func setMainListCellProtocol(mainListCellProtocol: MainListCellProtocol) {
        self.mainListCellProtocol = mainListCellProtocol
    }
    
    @objc public func cellButtonClick(_sender: UIButton) {
        if mainListCellProtocol != nil {
            mainListCellProtocol.onClick(index: self.index)
        }
    }
}
