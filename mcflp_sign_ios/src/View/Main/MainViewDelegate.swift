//
//  MainViewDelegate.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 11. 1..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation

protocol MainViewDelegate {
    func getListIndex(index: Int)
    func ozError(msg: String)
}
