//
//  LeftMenuView.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 1..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import UIKit
import SlideMenuControllerSwift

class LeftMenuView: UIViewController {
    
    private let menuWait: UIButton = UIButton()
    private let menuComplete: UIButton = UIButton()
    private let menuInfo: UIButton = UIButton()
    private let menuLogout: UIButton = UIButton()
    
    override func viewDidLoad() {
        setStatusBar()
        setBar()
    }
    
    private func setStatusBar() {
        self.view.frame = ((UIApplication.shared.delegate as! AppDelegate).window?.frame)!
        self.view.backgroundColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func setBar() {
        var strName: String = ""
        var setOrgName: String = ""
        
        if SaveDatas.LoginInfo["userName"].string != nil {
            strName = SaveDatas.LoginInfo["userName"].string!
        }
        
        if SaveDatas.LoginInfo["orgName"].string != nil {
            setOrgName = SaveDatas.LoginInfo["orgName"].string!
        }
        
        let bar = UIView()
        bar.frame = LeftMenuFrames.bar
        bar.backgroundColor = LeftMenuColors.bar
        
        self.view.addSubview(bar) 
        
        let logoBG = UIView()
        logoBG.frame = LeftMenuFrames.logoBG
        logoBG.backgroundColor = LeftMenuColors.logoBG
        logoBG.layer.cornerRadius = 20
        
//        self.view.addSubview(logoBG)
        
        let logo = LeftMenuImages.logo
        logo.frame = LeftMenuFrames.logo
        
//        self.view.addSubview(logo)
        
        let name = UILabel()
        name.frame = LeftMenuFrames.name
        name.text = strName
        name.textColor = LeftMenuColors.name
        name.font = LeftMenuFonts.name
        
        self.view.addSubview(name)
        
        let hospital = UILabel()
        hospital.frame = LeftMenuFrames.hospital
        hospital.text = setOrgName
        hospital.textColor = LeftMenuColors.hospital
        hospital.font = LeftMenuFonts.hospital
        
        self.view.addSubview(hospital)
        
        
        menuWait.frame = LeftMenuFrames.menuWait
//        menuWait.titleLabel?.frame = LeftMenuFrames.menuWait
        menuWait.addTarget(self, action: #selector(LeftMenuView.waitButtonClick(_sender:)), for: .touchUpInside)
        
        self.view.addSubview(menuWait)
        
        let titleWait = UILabel()
        titleWait.frame = LeftMenuFrames.menuWaitLabel
        titleWait.textColor = LeftMenuColors.manuWait
        titleWait.textAlignment = .left
        titleWait.text = LeftMenuStrings.menuWait
        
        self.view.addSubview(titleWait)
        
        
        menuComplete.frame = LeftMenuFrames.menuComplete
//        menuComplete.setTitle(LeftMenuStrings.menuComplete, for: .normal)
//        menuComplete.setTitleColor(LeftMenuColors.menuComplete, for: .normal)
//        menuComplete.titleLabel?.textAlignment = .left
        menuComplete.addTarget(self, action: #selector(LeftMenuView.completeButtonClick(_sender:)), for: .touchUpInside)
        
        self.view.addSubview(menuComplete)
        
        let titleComplete = UILabel()
        titleComplete.frame = LeftMenuFrames.menuCompleteLabel
        titleComplete.textColor = LeftMenuColors.menuComplete
        titleComplete.textAlignment = .left
        titleComplete.text = LeftMenuStrings.menuComplete
        
        self.view.addSubview(titleComplete)
        
        menuInfo.frame = LeftMenuFrames.menuInfo
//        menuInfo.setTitle(LeftMenuStrings.menuInfo, for: .normal)
//        menuInfo.setTitleColor(LeftMenuColors.menuInfo, for: .normal)
//        menuInfo.titleLabel?.textAlignment = .left
        menuInfo.addTarget(self, action: #selector(LeftMenuView.infoButtonClick(_sender:)), for: .touchUpInside)
        
        self.view.addSubview(menuInfo)
        
        let titleInfo = UILabel()
        titleInfo.frame = LeftMenuFrames.menuInfoLabel
        titleInfo.textColor = LeftMenuColors.menuInfo
        titleInfo.textAlignment = .left
        titleInfo.text = LeftMenuStrings.menuInfo
        
        self.view.addSubview(titleInfo)
        
        menuLogout.frame = LeftMenuFrames.menuLogout
//        menuLogout.setTitle(LeftMenuStrings.menuLogout, for: .normal)
//        menuLogout.setTitleColor(LeftMenuColors.menuLogout, for: .normal)
//        menuLogout.titleLabel?.textAlignment = .left
        menuLogout.addTarget(self, action: #selector(LeftMenuView.logoutButtonClick(_sender:)), for: .touchUpInside)
        
        self.view.addSubview(menuLogout)
        
        let titleLogout = UILabel()
        titleLogout.frame = LeftMenuFrames.menuLogoutLabel
        titleLogout.textColor = LeftMenuColors.menuLogout
        titleLogout.textAlignment = .left
        titleLogout.text = LeftMenuStrings.menuLogout
        
        self.view.addSubview(titleLogout)
        
        menuWait.backgroundColor = HexColor.hexStringToUIColor(hex: "ECECEC")
        menuComplete.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        menuInfo.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        menuLogout.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
    }
    
    @objc public func waitButtonClick(_sender: UIButton) {
        menuWait.backgroundColor = HexColor.hexStringToUIColor(hex: "ECECEC")
        menuComplete.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        menuInfo.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        menuLogout.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        
        let mainView = MainView()
        mainView.setCompleteList(isCompleteList: false)
        
        CertificationView.slideMenuController.changeMainViewController(mainView, close: true)
    }
    
    @objc public func completeButtonClick(_sender: UIButton) {
        menuWait.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        menuComplete.backgroundColor = HexColor.hexStringToUIColor(hex: "ECECEC")
        menuInfo.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        menuLogout.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        
        let mainView = MainView()
        mainView.setCompleteList(isCompleteList: true)
        
        CertificationView.slideMenuController.changeMainViewController(mainView, close: true)
    }
    
    @objc public func infoButtonClick(_sender: UIButton) {
        menuWait.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        menuComplete.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        menuInfo.backgroundColor = HexColor.hexStringToUIColor(hex: "ECECEC")
        menuLogout.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        
        let appInfoView = AppInfoView()
        let leftMenuView = LeftMenuView()
//        if CertificationView.slideMenuController.isLeftOpen() {
//            CertificationView.slideMenuController.closeLeft()
//        }
//        CertificationView.slideMenuController = SlideMenuController(mainViewController: appInfoView, leftMenuViewController: leftMenuView)
        
        CertificationView.slideMenuController.changeMainViewController(appInfoView, close: true)

//        self.present(CertificationView.slideMenuController, animated: true, completion: nil)
    }
    
    @objc public func logoutButtonClick(_sender: UIButton) {
        menuWait.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        menuComplete.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        menuInfo.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        menuLogout.backgroundColor = HexColor.hexStringToUIColor(hex: "FFFFFF")
        
        if CertificationView.slideMenuController.isLeftOpen() {
            CertificationView.slideMenuController.closeLeft()
        }
        
        let logout = UIAlertController(title: "로그아웃", message: "로그아웃 하시겠습니까?", preferredStyle: UIAlertControllerStyle.alert)
        
        logout.addAction(UIAlertAction(title: "취소", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
        })
        logout.addAction(UIAlertAction(title: "확인", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
             if UserDefaults.standard.string(forKey: "userSN") != nil {
                UserDefaults.standard.set(nil, forKey: "userSN")
            }
            
            let loginView = LoginView()
            
            self.present(loginView, animated:false, completion: nil)
        })
        
        present(logout, animated: true, completion: nil)
    }
}
