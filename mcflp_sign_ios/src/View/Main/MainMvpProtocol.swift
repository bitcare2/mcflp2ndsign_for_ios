//
//  MainMvpView.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 1..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol MainMvpProtocol {
    func activityIndicator(show: Bool)
    func apiGetFormList(listDatas: Array<MainListHaribo>)
    func getFormInfo(datas: JSON, index: Int)
}
