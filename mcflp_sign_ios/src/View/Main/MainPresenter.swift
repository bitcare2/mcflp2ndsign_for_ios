//
//  MainPresenter.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 1..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import SwiftyJSON

class MainPresenter {
    private var net: Net!
    private var mainMvpProtocol: MainMvpProtocol!
    
    private var formList: JSON!
    private var listDatas: Array<MainListHaribo> = Array<MainListHaribo>()
    
    init(mainMvpProtocol: MainMvpProtocol) {
        self.net = Net()
        self.mainMvpProtocol = mainMvpProtocol
    }
    
    public func apiGetFormList(isCompleteList: Bool) {
        self.mainMvpProtocol.activityIndicator(show: true)
        
        net.request(url: ApiDefine.getFormList,
                    params: net.getFormListParams(token: UserDefaults.standard.string(forKey: "token")!, startDate: "20180101", endDate: "20181212")) { (data) in
                        
                        print("data: \(data)")
                        
                        UserDefaults.standard.set(data["token"].string, forKey: "token")
                        self.formList = data["result"]
                        
                        print("size: \(self.formList.array?.count as! Int)")
                        
                        
                        if self.listDatas.count > 0 {
                            self.listDatas.removeAll()
                        }
                        
                        
                        for i in 0 ..< (self.formList.array?.count as! Int) {
                            if !isCompleteList {
                                if self.formList[i]["requestStateName"].string!.trimmingCharacters(in: .whitespacesAndNewlines) == "서명대기" ||
                                    self.formList[i]["requestStateName"].string!.trimmingCharacters(in: .whitespacesAndNewlines) == "최종확인대기" {
                                    self.listDatas.append(MainListHaribo(name: "\(self.formList[i]["userName"].string!)(\(self.formList[i]["userGender"].string!))",
                                                                    date: self.formList[i]["requestDate"].string!,
                                                                    type: self.formList[i]["formName"].string!,
                                                                    requestName: self.formList[i]["requestName"].string!.trimmingCharacters(in: .whitespacesAndNewlines),
                                                                    requestStateName: self.formList[i]["requestStateName"].string!.trimmingCharacters(in: .whitespacesAndNewlines),
                                                                    number: self.formList[i]["formNumber"].string!,
                                                                    signFormSN: self.formList[i]["signFormSN"].string!))
                                }
                            } else {
                                if self.formList[i]["requestStateName"].string!.trimmingCharacters(in: .whitespacesAndNewlines) == "등록완료" {
                                    self.listDatas.append(MainListHaribo(name: "\(self.formList[i]["userName"].string!)(\(self.formList[i]["userGender"].string!))",
                                                                    date: self.formList[i]["requestDate"].string!,
                                                                    type: self.formList[i]["formName"].string!,
                                                                    requestName: self.formList[i]["requestName"].string!.trimmingCharacters(in: .whitespacesAndNewlines),
                                                                    requestStateName: self.formList[i]["requestStateName"].string!.trimmingCharacters(in: .whitespacesAndNewlines),
                                                                    number: self.formList[i]["formNumber"].string!,
                                                                    signFormSN: self.formList[i]["signFormSN"].string!))
                                }
                            }
                        }
                        
                        self.listDatas.reverse()
                        
//                        self.listDatas = self.listDatas.sorted(by: {$0.getDate() > $1.getDate()})
                        
                        self.mainMvpProtocol.apiGetFormList(listDatas: self.listDatas)
//                        self.mainMvpProtocol.activityIndicator(show: false)
        }
    }
    
    public func getFormInfo(index: Int) {
        print("getFormInfo")
        
        
        for i in 0 ..< (self.listDatas.count as! Int) {
            print("name: \(self.listDatas[i].getName())")
            print("getSignFormSN: \(self.listDatas[i].getSignFormSN())")
        }
        
        
        print("index: \(index)")
        print("token: \(UserDefaults.standard.string(forKey: "token")!)")
        print("form: \(self.listDatas[index].getSignFormSN())")
        
        net.request(url: ApiDefine.getFormInfo,
                    params: net.getFormInfoParams(token: UserDefaults.standard.string(forKey: "token")!, signFormSN: self.listDatas[index].getSignFormSN())) { (data) in
                        print("data: \(data)")
                        
                        UserDefaults.standard.set(data["token"].string, forKey: "token")
                        
                        print("data: \(data["result"])")
                        
                        self.mainMvpProtocol.getFormInfo(datas: data, index: index)
        }
    }
    
    
}
