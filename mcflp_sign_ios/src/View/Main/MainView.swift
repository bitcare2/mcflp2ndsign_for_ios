//
//  MainView.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 1..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import DropDown
import SwiftSVG

class MainView: UIViewController, UITableViewDelegate, UITableViewDataSource, MainListCellProtocol, MainMvpProtocol, MainViewDelegate {
    
    private var mainPresenter: MainPresenter!
    private var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    private var dropdown: DropDown = DropDown()
    
    private var tableView: UITableView!
    private var listDatas: Array<MainListHaribo> = Array<MainListHaribo>()
//    private var listDatas = [MainListHaribo]()
    
//    private var openSignFormSN: String = "close"
    private var isCompleteList: Bool = false
    
    private let titleLabel = UILabel()
    
    override func viewDidLoad() {
        mainPresenter = MainPresenter(mainMvpProtocol: self)
        
        setStatusBar()
        setTitlebar()
        setMenu()
        
//        if self.openSignFormSN != "close" {
//            mainPresenter.getFormInfo(signFormSN: self.openSignFormSN)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear")
        
//        if self.openSignFormSN == "close" {
            mainPresenter.apiGetFormList(isCompleteList: self.isCompleteList)
//        }
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ScreenInfo.getHeight(height: 130)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listDatas.count
//        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = MainListCell()
        cell.setData(mainListHaribo: listDatas[indexPath.row])
        cell.setIndex(index: indexPath.row)
        cell.setMainListCellProtocol(mainListCellProtocol: self)
        
//        print("listDatas.count: \(listDatas.count)")
//        print("indexPath.count: \(indexPath.row)")
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("test: \(indexPath)")
    }
    
    func onClick(index: Int) {
        self.mainPresenter.getFormInfo(index: index)
//        print("menu Click: \(index)")
    }
    
    func activityIndicator(show: Bool) {
        print("activityIndicator")
        if show {
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            
            activityIndicator.frame = CGRect(x: view.frame.midX-50, y: view.frame.midY-50, width: 100, height: 100)
            activityIndicator.color = UIColor.red
            activityIndicator.hidesWhenStopped = true
            activityIndicator.startAnimating()
            self.view.addSubview(activityIndicator)
        } else {
            self.activityIndicator.removeFromSuperview()
        }
    }
    
    func apiGetFormList(listDatas: Array<MainListHaribo>) {
        self.listDatas = listDatas
        setList()
    }
    
    func getFormInfo(datas: JSON, index: Int) {
        print("getFormInfo")
        
        print("datas: \(datas)")
        
        if datas == nil || datas["result"].isEmpty || datas["userInfo"].isEmpty || datas["formType"].isEmpty {
            let dialog = UIAlertController(title: "error", message: "상세정보가 없습니다.", preferredStyle: .alert)
            let action = UIAlertAction(title: "확인", style: .default)
            
            dialog.addAction(action)
            
            self.present(dialog, animated: true, completion: nil)
        } else {
            let ozview = OZView()
            ozview.setData(datas: datas)
//            ozview.setTitle(title: name)
            ozview.setMainViewDelegate(mainViewDelegate: self, index: index)
            self.present(ozview, animated: false, completion: nil)
        }
        
//        self.openSignFormSN = "close"

    }
    
    func getListIndex(index: Int) {
        if tableView != nil {
            let index = IndexPath(row: index, section: 0)
            
            tableView.scrollToRow(at: index, at: .none, animated: true)
        }
    }
    
    func ozError(msg: String) {
        let dialog = UIAlertController(title: "error", message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "확인", style: .default)
        
        dialog.addAction(action)
        
        self.present(dialog, animated: true, completion: nil)
    }
    
//    public func setFormOpen(openSignFormSN: String) {
//        self.openSignFormSN = openSignFormSN
//    }
    
    public func setCompleteList(isCompleteList: Bool) {
        self.isCompleteList = isCompleteList
    }
    
    
    private func setStatusBar() {
        self.view.frame = ((UIApplication.shared.delegate as! AppDelegate).window?.frame)!
        self.view.backgroundColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = true
        
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = HexColor.hexStringToUIColor(hex: "#4380DB")
        statusBarView.backgroundColor = statusBarColor
        self.view.addSubview(statusBarView)
    }
    
    private func setTitlebar() {
//        let bar = MainImages.bar
        let bar = UIView()
        bar.backgroundColor = HexColor.hexStringToUIColor(hex: "#2967C2")
          bar.frame = MainFrames.bar
        self.view.addSubview(bar)
        
        let menu = UIButton()
        menu.frame = MainFrames.menu
//        menu.setTitle("=", for: .normal)
//        menu.setTitleColor(HexColor.hexStringToUIColor(hex: "#FFFFFF"), for: .normal)
//        menu.titleLabel?.font = UIFont(name: "NanumGothicBold", size: ScreenInfo.getWidth(width:40))!
        menu.setImage(UIImage(named: "icon_menu"), for: .normal)
        menu.addTarget(self, action: #selector(MainView.menuButtonClick(_sender:)), for: .touchUpInside)
        
        self.view.addSubview(menu)
        
        titleLabel.frame = MainFrames.titleLabel
        titleLabel.textColor = MainColors.titleLabel
        titleLabel.font = MainFonts.titleLabel
        titleLabel.adjustsFontSizeToFitWidth = true
        
        self.view.addSubview(titleLabel)
        
        let refresh = UIButton()
        refresh.frame = CGRect(x: ScreenInfo.currentDeviceWidth - ScreenInfo.getWidth(width: 90),
                               y: UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 15),
                               width: ScreenInfo.getWidth(width: 30),
                               height: ScreenInfo.getHeight(height: 30))
        refresh.setImage(UIImage(named: "icon_refresh"), for: .normal)
        refresh.addTarget(self, action: #selector(MainView.refreshButtonClick(_sender:)), for: .touchUpInside)
        
        self.view.addSubview(refresh)
        
        
//        let test = SVGView(SVGNamed: "icon_menu")
//        test.frame = CGRect(x: ScreenInfo.getWidth(width:15),
//                            y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 15),
//                            width: ScreenInfo.getWidth(width: 28),
//                            height: ScreenInfo.getHeight(height: 28))
//
//        self.view.addSubview(test)
    }
    
    private func setList() {
        if !isCompleteList {
            titleLabel.text = MainStrings.titleLabel1 + " (\(listDatas.count))"
        } else {
            titleLabel.text = MainStrings.titleLabel2 + " (\(listDatas.count))"
        }
        
        if listDatas.count == 0 {
            let bgNodata = UIImageView(image: UIImage(named: "bg_nodata"))
            bgNodata.frame = CGRect(x:0,
                                    y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 60),
                                    width: ScreenInfo.currentDeviceWidth,
                                    height: ScreenInfo.currentDeviceHeight - (UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 60)))
            
            
            self.view.addSubview(bgNodata)
            
        } else {
            tableView = UITableView()
            
            tableView.frame = MainFrames.list
            tableView.backgroundColor = UIColor.white
            tableView.showsVerticalScrollIndicator = false
            tableView.delegate = self
            tableView.dataSource = self
            tableView.allowsSelection = false
            //        tableView.separatorColor = .black
            //        tableView.separatorStyle = .singleLine
            tableView.separatorStyle = .none
            
            self.view.addSubview(tableView)
        }
        
        
        
        
        
//        tableView.translatesAutoresizingMaskIntoConstraints = false
//        tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
//        tableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
//        tableView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1).isActive = true
//        tableView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 1).isActive = true
        
        
    }
    
    private func setMenu() {
        
        let menuButton = UIButton()
//        menuButton.setTitle(":", for: .normal)
//        menuButton.setTitleColor(.white, for: .normal)
//        menuButton.titleLabel?.font = MainFonts.rightMenu
        menuButton.frame = MainFrames.rightMenu
        menuButton.setImage(UIImage(named: "icon_more"), for: .normal)
        menuButton.addTarget(self, action: #selector(MainView.rightmenuButtonClick(_sender:)), for: .touchUpInside)
        
        self.view.addSubview(menuButton)
        
        let hideButton = UIButton()
        hideButton.frame = MainFrames.hiddenrightMenu
        
        self.view.addSubview(hideButton)
        
        let titles = ["1개월", "3개월", "6개월", "12개월", "전체"]
        
        self.dropdown.anchorView = hideButton
        self.dropdown.backgroundColor = .white
        self.dropdown.dataSource = titles
        self.dropdown.direction = .bottom
        
        self.dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("index: \(index), item: \(item)")
            self.dropdown.clearSelection()
            
        }
    }
    
    @objc public func menuButtonClick(_sender: UIButton) {
        if CertificationView.slideMenuController.isLeftOpen() {
            CertificationView.slideMenuController.closeLeft()
        } else {
            CertificationView.slideMenuController.openLeft()
        }
    }
    
    @objc public func refreshButtonClick(_sender: UIButton) {
        mainPresenter.apiGetFormList(isCompleteList: self.isCompleteList)
    }
    
    @objc public func rightmenuButtonClick(_sender: UIButton) {
        self.dropdown.selectionBackgroundColor = .lightGray
        self.dropdown.reloadAllComponents()
        dropdown.show()
    }

}
