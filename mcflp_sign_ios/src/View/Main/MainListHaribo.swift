//
//  MainListHaribo.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 10..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation

class MainListHaribo {
    private var name: String!
    private var date: String!
    private var type: String!
    private var requestName: String!
    private var requestStateName: String!
    private var number: String!
    private var signFormSN: String!
    
    init(name: String, date: String, type: String, requestName: String, requestStateName: String, number: String, signFormSN: String) {
        self.name = name
        self.date = date
        self.type = type
        self.requestName = requestName
        self.requestStateName = requestStateName
        self.number = number
        self.signFormSN = signFormSN
    }
    
    public func getName() -> String {
        return self.name
    }
    
    public func getDate() -> String {
        return self.date
    }
    
    public func getType() -> String {
        return self.type
    }
    
    public func getRequestName() -> String {
        return self.requestName
    }
    
    public func getRequestStateName() -> String {
        return self.requestStateName
    }
    
    public func getNumber() -> String {
        return self.number
    }
    
    public func getSignFormSN() -> String {
        return self.signFormSN
    }
}
