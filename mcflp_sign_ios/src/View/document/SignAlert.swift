//
//  SignAlert.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 10..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import Toaster

protocol SignAlertAction {
    func finish()
}

protocol Modal {
    func show(animated: Bool)
    func dismiss(animated: Bool)
    var backgroundView: UIView {get}
    var dialogView: UIView {get set}
}


class SignAlert: UIView, Modal, UITextViewDelegate, UITextFieldDelegate {
    private let net: Net = Net()
    fileprivate var signAlertAction: SignAlertAction!
    
    var backgroundView: UIView = UIView()
    var dialogView = UIView()
    var sketchView = SketchView()
    
    fileprivate let title = UILabel()
    
    fileprivate let comment = UITextField()
    fileprivate let dateField = UITextField()
    
    fileprivate var datePicker: UIDatePicker!
    
    fileprivate var name: String!
    fileprivate var regday: String!
    
    fileprivate var signFormSN: String!
    
    fileprivate var isComment: Bool!
    fileprivate var cancelType: Bool!
    
    
    private var saveY: CGFloat!
    
    convenience init(isComment: Bool, name: String, cancelType: Bool, strComment: String, strDate: String) {
        self.init(frame: UIScreen.main.bounds)
        
        self.isComment = isComment
        initalize(isComment: isComment, name: name, cancelType: cancelType, strComment: strComment, strDate: strDate)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         print("touchesBegan")
        self.endEditing(true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func keyboardWillShow(_ sender: Notification) {
        if self.dialogView.frame.origin.y != -150 {
            self.saveY = self.dialogView.frame.origin.y
        }
        
        self.dialogView.frame.origin.y = -150 // Move view 150 points upward
    }
    
    func keyboardWillHide(_ sender: Notification) {
        self.dialogView.frame.origin.y = self.saveY // Move view to original position
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    private func initalize(isComment: Bool, name: String, cancelType: Bool, strComment: String, strDate: String) {
        print("initalize")
        
        self.cancelType = cancelType
        
        dialogView.clipsToBounds = true
        
        backgroundView.frame = frame
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedOnBackgroundView)))
        
        addSubview(backgroundView)
        
        dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
        if isComment {
            dialogView.frame.size = CGSize(width: ScreenInfo.getWidth(width: 300), height: ScreenInfo.getHeight(height: 450))
        } else {
            dialogView.frame.size = CGSize(width: ScreenInfo.getWidth(width: 300), height: ScreenInfo.getHeight(height: 300))
        }
        
        dialogView.backgroundColor = UIColor.white
        dialogView.layer.cornerRadius = ScreenInfo.getWidth(width: 5)
        
        addSubview(dialogView)
        
        title.frame = CGRect(x: ScreenInfo.getWidth(width: 10), y: ScreenInfo.getHeight(height: 20), width: ScreenInfo.getWidth(width: 100), height: ScreenInfo.getHeight(height: 30))
        title.text = " asdasjdlakjsdlasjdlkajsdlkajdl"
        title.font = UIFont(name: "NanumGothicBold", size: ScreenInfo.getHeight(height: 20))
        title.sizeToFit()
        
        dialogView.addSubview(title)
        
        sketchView.frame = CGRect(x: ScreenInfo.getHeight(height: 10), y: ScreenInfo.getHeight(height: 60), width: dialogView.frame.width - ScreenInfo.getHeight(height: 20), height: ScreenInfo.getHeight(height: 150))
        sketchView.backgroundColor = HexColor.hexStringToUIColor(hex: "#FFFFFF")
        sketchView.layer.masksToBounds = true
        sketchView.layer.cornerRadius = ScreenInfo.getWidth(width: 4)
        sketchView.layer.borderWidth = ScreenInfo.getWidth(width: 1)
        sketchView.layer.borderColor = HexColor.hexStringToUIColor(hex: "#ACACAC").cgColor
        
        dialogView.addSubview(sketchView)
        
        let txtLabel = UILabel()
        txtLabel.frame = CGRect(x: ScreenInfo.getWidth(width: 10), y: ScreenInfo.getHeight(height: 220), width: ScreenInfo.getWidth(width: 100), height: ScreenInfo.getHeight(height: 30))
        txtLabel.text = "환자명: \(name)"
        txtLabel.sizeToFit()
        
        dialogView.addSubview(txtLabel)
        
        let clear = UIButton()
        clear.setTitle("지우기", for: .normal)
        clear.setTitleColor(HexColor.hexStringToUIColor(hex: "#2967C2"), for: .normal)
        clear.addTarget(self, action: #selector(SignAlert.clearButtonClick(_sender:)), for: .touchUpInside)
        
        dialogView.addSubview(clear)
        
        let confirm = UIButton()
        confirm.setTitle("확인", for: .normal)
        confirm.setTitleColor(HexColor.hexStringToUIColor(hex: "#2967C2"), for: .normal)
        confirm.addTarget(self, action: #selector(SignAlert.confirmButtonClick(_sender:)), for: .touchUpInside)
        
        
        dialogView.addSubview(confirm)
        
        let cancel = UIButton()
        cancel.setTitle("취소", for: .normal)
        cancel.setTitleColor(HexColor.hexStringToUIColor(hex: "#2967C2"), for: .normal)
        cancel.addTarget(self, action: #selector(SignAlert.cancelButtonClick(_sender:)), for: .touchUpInside)
        
        dialogView.addSubview(cancel)
        
        if isComment {
            title.text = "전문의 서명하기"
            
            
            
            let date = UILabel()
            date.frame = CGRect(x: ScreenInfo.getWidth(width: 10), y: ScreenInfo.getHeight(height: 250), width: ScreenInfo.getWidth(width: 100), height: ScreenInfo.getHeight(height: 30))
            date.text = "판단일"
            date.sizeToFit()
            
            dialogView.addSubview(date)
            
            dateField.frame = CGRect(x: ScreenInfo.getWidth(width: 10), y: ScreenInfo.getHeight(height: 280), width: ScreenInfo.getWidth(width: 280), height: ScreenInfo.getHeight(height: 40))
            dateField.leftViewMode = UITextFieldViewMode.always
            dateField.leftView = UIView(frame: SignAlertFrames.txtField_leftView)
            dateField.delegate = self
            dateField.layer.borderColor = HexColor.hexStringToUIColor(hex: "#ACACAC").cgColor
            dateField.layer.borderWidth = ScreenInfo.getWidth(width: 1)
            dateField.layer.cornerRadius = ScreenInfo.getWidth(width: 5)
            dateField.font = UIFont(name: "NanumGothic", size: ScreenInfo.getWidth(width:14))!
            dateField.text = strDate
            
            dialogView.addSubview(dateField)
            
            setDatePicker()
            
            let commentLabel = UILabel()
            commentLabel.frame = CGRect(x: ScreenInfo.getWidth(width: 10), y: ScreenInfo.getHeight(height: 330), width: ScreenInfo.getWidth(width: 100), height: ScreenInfo.getHeight(height: 30))
            commentLabel.text = "판단내용"
            commentLabel.sizeToFit()
            
            dialogView.addSubview(commentLabel)
            
            comment.frame = CGRect(x: ScreenInfo.getWidth(width: 10), y: ScreenInfo.getHeight(height: 355), width: ScreenInfo.getWidth(width: 280), height: ScreenInfo.getHeight(height: 40))
            comment.leftViewMode = UITextFieldViewMode.always
            comment.leftView = UIView(frame: SignAlertFrames.txtField_leftView)
            comment.delegate = self
            comment.layer.borderColor = HexColor.hexStringToUIColor(hex: "#ACACAC").cgColor
            comment.layer.borderWidth = ScreenInfo.getWidth(width: 1)
            comment.layer.cornerRadius = ScreenInfo.getWidth(width: 5)
            comment.text = strComment
            
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
            
            dialogView.addSubview(comment)
            
            clear.frame = CGRect(x: ScreenInfo.getWidth(width: 10), y: ScreenInfo.getHeight(height: 400), width: ScreenInfo.getWidth(width: 80), height: ScreenInfo.getHeight(height: 50))
            confirm.frame = CGRect(x: ScreenInfo.getWidth(width: 175), y: ScreenInfo.getHeight(height: 400), width: ScreenInfo.getWidth(width: 40), height: ScreenInfo.getHeight(height: 50))
            cancel.frame = CGRect(x: ScreenInfo.getWidth(width: 240), y: ScreenInfo.getHeight(height: 400), width: ScreenInfo.getWidth(width: 40), height: ScreenInfo.getHeight(height: 50))
            
        } else {
            if self.cancelType! == true {
                title.text = "환자 철회하기"
            } else {
                title.text = "환자 서명하기"
            }
            
            clear.frame = CGRect(x: ScreenInfo.getWidth(width: 10), y: ScreenInfo.getHeight(height: 250), width: ScreenInfo.getWidth(width: 80), height: ScreenInfo.getHeight(height: 50))
            confirm.frame = CGRect(x: ScreenInfo.getWidth(width: 175), y: ScreenInfo.getHeight(height: 250), width: ScreenInfo.getWidth(width: 40), height: ScreenInfo.getHeight(height: 50))
            cancel.frame = CGRect(x: ScreenInfo.getWidth(width: 240), y: ScreenInfo.getHeight(height: 250), width: ScreenInfo.getWidth(width: 40), height: ScreenInfo.getHeight(height: 50))
        }
    }
    
    fileprivate func setDatePicker() {
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        dateField.inputView = self.datePicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "확인", style: .plain, target: self, action: #selector(SignAlert.donePressed))
        doneButton.tintColor = HexColor.hexStringToUIColor(hex: "#000000")
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "취소", style: .plain, target: self, action: #selector(SignAlert.cancelPressed))
        cancelButton.tintColor = HexColor.hexStringToUIColor(hex: "#000000")
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        dateField.inputAccessoryView = toolBar
    }
    
    public func setRegDay(regday: String) {
        self.regday = regday
    }
    
    public func setSignFormSN(signFormSN: String) {
        self.signFormSN = signFormSN
    }
    
//    public func setCancelType(cancelType: Bool) {
//        self.cancelType = cancelType
//
//        print("self.cancelType: \(self.cancelType!)")
//
//
//    }
    
    public func setSignAlertAction(signAlertAction: SignAlertAction) {
        self.signAlertAction = signAlertAction
    }
    
    @objc public func clearButtonClick(_sender: UIButton) {
        sketchView.clear()
    }
    
    @objc public func confirmButtonClick(_sender: UIButton) {
        
        if self.isComment {
            if self.regday != nil {
                print("dateFormat: \(DateFormat.toString(before: "yyyy-MM-dd", after: "yyyyMMdd", date: self.dateField.text!))")
                print("regdayFormat: \(DateFormat.toString(before: "yyyy-MM-dd", after: "yyyyMMdd", date: self.regday))")
                
                let day: Int = Int(DateFormat.toString(before: "yyyy-MM-dd", after: "yyyyMMdd", date: self.dateField.text!))!
                let regday: Int = Int(DateFormat.toString(before: "yyyy-MM-dd", after: "yyyyMMdd", date: self.regday))!
                
                if  day > regday {
                    let toast = Toast(text: "판단일이 작성일보다 미래일 수 없습니다.", duration: Delay.short)
                    toast.show()
                    
                    return
                }
            }
        }
        
        if self.cancelType {
            net.uploadPNG(url: ApiDefine.addCancelSignImg,
                          img: sketchView,
                          params: net.addCancelSignImg(token: UserDefaults.standard.string(forKey: "token")!, signFormSN: self.signFormSN)) { (data) in
                            print("data: \(data)")
                            
                            if data["serviceCode"] == 100 {
                                print("token: \(data["token"].string)")
                                UserDefaults.standard.set(data["token"].string, forKey: "token")
                                
                                if self.signAlertAction != nil {
                                    self.signAlertAction.finish()
                                }
                            }
            }
        } else {
            net.uploadPNG(url: ApiDefine.addSignImg,
                          img: sketchView,
                          params: net.getAddSignImg(token: UserDefaults.standard.string(forKey: "token")!, signFormSN: self.signFormSN)) { (data) in
                            print("data: \(data)")
                            
                            if data["serviceCode"] == 100 {
                                print("token: \(data["token"].string)")
                                UserDefaults.standard.set(data["token"].string, forKey: "token")
                                
                                if self.signAlertAction != nil {
                                    self.signAlertAction.finish()
                                }
                            }
            }
        }
    }
    
    @objc public func cancelButtonClick(_sender: UIButton) {
        dismiss(animated: true)
    }
    
    @objc public func donePressed(_ sender : Any) {
        //format date
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        dateField.text = dateFormatter.string(from: datePicker.date)
        self.endEditing(true)
    }
    
    @objc public func cancelPressed(_ sender : Any) {
        self.endEditing(true)
    }
    
    @objc func didTappedOnBackgroundView() {
//        dismiss(animated: true)
//        self.dialogView.endEditing(true)
        self.endEditing(true)
    }
}

extension Modal where Self: UIView {
    func show(animated: Bool) {
        self.backgroundView.alpha = 0
        if var topController = UIApplication.shared.delegate?.window??.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.view.addSubview(self)
        }
        
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.backgroundView.alpha = 0.66
            })
            
            UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 10, options: UIViewAnimationOptions(rawValue: 0), animations: {
                self.dialogView.center = self.center
            }, completion: { (completed) in
                
            })
        } else {
            self.backgroundView.alpha = 0.66
            self.dialogView.center = self.center
        }
    }
    
    func dismiss(animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.backgroundView.alpha = 0
            }, completion: { (completed) in
                
            })
            
            UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIViewAnimationOptions(rawValue: 0), animations: {
                self.dialogView.center = CGPoint(x: self.center.x, y: self.frame.height + self.dialogView.frame.height / 2)
            }, completion: { (completed) in
                self.removeFromSuperview()
            })
        } else {
            self.removeFromSuperview()
        }
    }
}
