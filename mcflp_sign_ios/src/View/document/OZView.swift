//
//  OZView.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 5..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import UIKit
import Material
import TextAttributes
import SwiftyJSON
import Material
import Alamofire
import SlideMenuControllerSwift

class OZView: UIViewController , OZReportCommandListener, SignAlertAction {
    public static let VIEW_TYPE_NORMAL: Int = 0
    public static let VIEW_TYPE_LOGINUSER: Int = 1
    public static let VIEW_TYPE_LOGINNOUSER: Int = 2
    
    /// 오즈 뷰어
    private var viewer: OZReportViewer!
    /// 뷰어에 전달할 데이터
    public var data: [String: Any?] = [:]
    private var datas: JSON!
    
    /// 문서 상태 코드
    public var lstplanstatcode: String = "01"
    
    /// 신규작성여부
    fileprivate var isFirst: Bool = true
    /// 등록여부
    fileprivate var isRegist: Bool = false
    fileprivate var lstplantempsn: String = ""
    
    fileprivate var target: String = ""
    fileprivate var max: Int = 0
    fileprivate var isPhone: String = ""
    
    //뷰 종류 0: 메인에서 들어옴, 1: 비회원 아닐 때 로그인에서 들어옴, 2: 비회원일 때 로그인에서 들어옴
    fileprivate var viewType: Int = OZView.VIEW_TYPE_NORMAL
    
    private var saveIndex: Int = 0
    
    private var titleLabel: String = ""
    
    private var formUrl: String!
    
    private var mainViewDelegate: MainViewDelegate?
    
    @objc func createView(){
        print("createView")
        let reportView = UIView()
        reportView.frame = OZViewFrames.ozview
        self.view.addSubview(reportView)

        print("formUrl: \(self.formUrl as! String)")
        print("jsonData: \(self.datas["result"].rawString([writingOptionsKeys.castNilToNSNull : true])!)")
        print("userInfo: \(self.datas["userInfo"].rawString([writingOptionsKeys.castNilToNSNull : true])!)")
        
//        userDetails.getOrgnm() + " " + userDetails.getMemberName() + "(" + userDetails.getUserSN() + ") " + strCT
        
        let today = Date() //현재 시각 구하기
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        var dateString = dateFormatter.string(from: today)
        print(dateString) //"2016년 2월 21일"
        
        var orgmednm: String = ""
        var usernm: String = ""
        var usersn: String = ""
        
        if self.datas["userInfo"]["orgmednm"] != nil {
            orgmednm = self.datas["userInfo"]["orgmednm"].string!
        }
        
        if self.datas["userInfo"]["usernm"] != nil {
            usernm = self.datas["userInfo"]["usernm"].string!
        }
        if self.datas["userInfo"]["usersn"] != nil {
            usersn = self.datas["userInfo"]["usersn"].string!
        }
        
        var userInfo: String = "\(orgmednm) \(usernm) + (\(usersn)) \(dateString)"
        
        print("test: \(self.datas["userInfo"]["signimgfilegrpno"].string)")
        
        let strParam = """
        connection.servlet=http://14.36.46.131:8480/oz70/server
        connection.reportname=/web\(formUrl as! String)
        connection.pcount=2
        connection.args1=jsondata=\(self.datas["result"].rawString([writingOptionsKeys.castNilToNSNull : true])!)
        connection.args2=userInfo=\(userInfo)
        viewer.errorcommand=true
        """
        
        let reportViewer: OZReportViewer? = OZReportAPI.createViewer(self, view: reportView, listener: self, param:strParam, delimiter:"\n", closeButton: "" )
        viewer = reportViewer
        
//        viewer.zoombydoubletap=false
//        viewer.minzoom=30
//        viewer.usetoolbar=false
//        viewer.progresscommand=true
//        eform.signpad_type=zoom
        
//        connection.servlet=https://report.lst.go.kr:443/oz70/server
//        connection.servlet=http://59.10.164.94:8080/oz70/server
//        connection.reportname=/realused\(formUrl as! String)
//        connection.args1=jsonData=\(JSON(data).rawString([writingOptionsKeys.castNilToNSNull : true])!)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    /// 뷰가 나타난 이후
    /// 통신은 여기에서함.
    /// - Parameter animated:
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setStatusBar()
        setTitlebar()
        
        perform(#selector(self.createView), with:nil, afterDelay: 0.1)
    }
    
    //여기서부터 리스너
    func ozCloseCommand() {
        print("ozCloseCommand")
    }
    func ozExitCommand() {
        print("ozExitCommand")
        
    }
    func ozErrorCommand(_ code: String!, _ message: String!, _ detailmessage: String!, _ reportname: String!) {
        print("code: \(code)")
        print("message: \(message)")
        print("detailmessage: \(detailmessage)")
        print("reportname: \(reportname)")
        self.dismiss(animated: true, completion: {() in
            if self.mainViewDelegate != nil {
                self.mainViewDelegate?.ozError(msg: message)
            }
            
        })
        
    }
    func ozUserActionCommand(_ type: String!, _ attr: String!) {
        //        Logger.info(message: type + " : " + attr)
    }
    func ozExportCommand(_ code: String!, _ path: String!, _ filename: String!, _ pagecount: String!, _ filepaths: String!) {
        
    }
    func ozeFormInputEventCommand(_ docindex: String!, _ formid: String!, _ eventname: String!, _ mainscreen: String!) {
        
    }
    func ozPageChangeCommand(_ docindex: String!) {
        
    }
    func ozReportChangeCommand(_ docindex: String!) {
        
    }
    func ozProgressCommand(_ step: String!, _ state: String!, _ reportname: String!) {
        if step == "4" && state == "2" {
            //            bClose.isEnabled = true
            //            bButton2.isEnabled = true
            //            bButton1.isEnabled = true
        }
    }
    func ozExportMemoryStreamCallBack(_ outputdata: String!) {
        
    }
    
    func ozUserEvent(_ param1: String!, _ param2: String!, _ param3: String!) -> String! {
        
        return "";
    }
    
    func finish() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func setStatusBar() {
        self.view.frame = ((UIApplication.shared.delegate as! AppDelegate).window?.frame)!
        self.view.backgroundColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = true
        
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = HexColor.hexStringToUIColor(hex: "#4380DB")
        statusBarView.backgroundColor = statusBarColor
        self.view.addSubview(statusBarView)
    }
    
    private func setTitlebar() {
//        let bar = OZViewImages.bar
        let bar = UIView()
        bar.backgroundColor = HexColor.hexStringToUIColor(hex: "#2967C2")
        bar.frame = OZViewFrames.bar
        self.view.addSubview(bar)
        
        let back = UIButton()
        back.frame = OZViewFrames.back
//        back.setTitle("<", for: .normal)
//        back.setTitleColor(HexColor.hexStringToUIColor(hex: "#FFFFFF"), for: .normal)
//        back.titleLabel?.font = UIFont(name: "NanumGothicBold", size: ScreenInfo.getWidth(width:40))!
        back.setImage(UIImage(named: "icon_back"), for: .normal)
        back.addTarget(self, action: #selector(OZView.backButtonClick(_sender:)), for: .touchUpInside)
        
        self.view.addSubview(back)
        
        let titleLabel = UILabel()
        titleLabel.frame = OZViewFrames.titleLabel
        titleLabel.text = self.titleLabel
        titleLabel.textColor = OZViewColors.titleLabel
        titleLabel.font = OZViewFonts.titleLabel
        titleLabel.adjustsFontSizeToFitWidth = true
        
        self.view.addSubview(titleLabel)
        
        if self.viewType == OZView.VIEW_TYPE_LOGINNOUSER {
            let sign = UIButton()
            sign.frame = OZViewFrames.sign
            
            if self.datas["formType"]["requestState"].string == "01" {
                sign.setTitle("서명하기", for: .normal)
                sign.addTarget(self, action: #selector(OZView.signButtonClick(_sender:)), for: .touchUpInside)
            } else {
                sign.setTitle("서명완료", for: .normal)
            }
            
            sign.setTitleColor(HexColor.hexStringToUIColor(hex: "#FFFFFF"), for: .normal)
            sign.titleLabel?.font = UIFont(name: "NanumGothicBold", size: ScreenInfo.getWidth(width:18))!
            
            self.view.addSubview(sign)
        } else {
            if self.datas["formType"]["requestState"].string == "01" {
                let sign = UIButton()
                sign.frame = OZViewFrames.sign
                
                if self.datas["formType"]["cancelType"].string == "Y" {
                    sign.setTitle("철회하기", for: .normal)
                } else {
                    sign.setTitle("서명하기", for: .normal)
                }
                
                
                sign.setTitleColor(HexColor.hexStringToUIColor(hex: "#FFFFFF"), for: .normal)
                sign.titleLabel?.font = UIFont(name: "NanumGothicBold", size: ScreenInfo.getWidth(width:18))!
                sign.addTarget(self, action: #selector(OZView.signButtonClick(_sender:)), for: .touchUpInside)
                
                self.view.addSubview(sign)
            }
        }
    }
    
    public func setData(datas: JSON) {
        print("setData")
        self.datas = datas
        
        /*
         "01"-> "연명의료 계획서"
         "02"-> "사전연명의료 의향서"
         "03"-> "임종과정에 있는 환자 판단서"
         "04"-> "환자의사 확인서(사전연명의료의향서)"
         "05"-> "환자가족 진술서"
         "06"-> "친권자 및 환자가족 의사 확인서"
         "07"-> "이행서"
 */
        
        if self.datas["formType"]["formCode"] == "01" {
            //연명의료 계획서
            formUrl = "/lstplan_v1.ozr"
            if datas["result"]["patiusernm"].string != nil {
                titleLabel = datas["result"]["patiusernm"].string!
            } else {
                titleLabel = ""
            }
        } else if self.datas["formType"]["formCode"] == "02" {
            //사전연명의료 의향서
            formUrl = "/addt_v1.ozr"
            if datas["result"]["wrtusernm"].string != nil {
                titleLabel = datas["result"]["wrtusernm"].string!
            } else {
                titleLabel = ""
            }
        } else if self.datas["formType"]["formCode"] == "03" {
            //임종과정에 있는 환자 판단서
            formUrl = "/edfjatwapid_v1.ozr"
            if datas["result"]["patiusernm"].string != nil {
                titleLabel = datas["result"]["patiusernm"].string!
            } else {
                titleLabel = ""
            }
        } else if self.datas["formType"]["formCode"] == "04" {
            //환자의사 확인서(사전연명의료의향서)
            formUrl = "/edfvopiaddt_v1.ozr"
            if datas["result"]["patiusernm"].string != nil {
                titleLabel = datas["result"]["patiusernm"].string!
            } else {
                titleLabel = ""
            }
        } else if self.datas["formType"]["formCode"] == "05" {
            //환자가족 진술서
            formUrl = "/edfvopisotpf_v1.ozr"
            if datas["result"]["patiusernm"].string != nil {
                titleLabel = datas["result"]["patiusernm"].string!
            } else {
                titleLabel = ""
            }
        } else if self.datas["formType"]["formCode"] == "06" {
            //친권자 및 환자가족 의사 확인서
            formUrl = "/edffamconfr_v1.ozr"
            if datas["result"]["edffamconfrDetailInfo"]["patiusernm"].string != nil {
                titleLabel = datas["result"]["edffamconfrDetailInfo"]["patiusernm"].string!
            } else {
                titleLabel = ""
            }
        }
    }
    
    public func setViewType(type: Int) {
        self.viewType = type
    }
    
    public func setMainViewDelegate(mainViewDelegate: MainViewDelegate, index: Int) {
        self.mainViewDelegate = mainViewDelegate
        self.saveIndex = index
    }
    
    @objc public func backButtonClick(_sender: UIButton) {
        
        if self.viewType == OZView.VIEW_TYPE_NORMAL {
            //메인에서 들어옴
            self.dismiss(animated: false, completion: {() in
                if self.mainViewDelegate != nil {
                    self.mainViewDelegate?.getListIndex(index: self.saveIndex)
                }
                
            })
        } else if self.viewType == OZView.VIEW_TYPE_LOGINUSER {
            //비회원 아닐 때 로그인에서 들어옴
            let mainView = MainView()
            let leftMenuView = LeftMenuView()
            
            CertificationView.slideMenuController = SlideMenuController(mainViewController: mainView, leftMenuViewController: leftMenuView)
            
            self.present(CertificationView.slideMenuController, animated: false, completion: nil)
        } else if self.viewType == OZView.VIEW_TYPE_LOGINNOUSER {
            //비회원일 때 로그인에서 들어옴
            let dialog = UIAlertController(title: "나가기", message: "msg", preferredStyle: .alert)
            let action = UIAlertAction(title: "확인", style: .default) {(action: UIAlertAction) -> Void in
                self.dismiss(animated: false, completion: nil)
            }
            
            dialog.addAction(action)
            self.present(dialog, animated: true, completion: nil)
        }
        
    }
    
    @objc public func signButtonClick(_sender: UIButton) {
        var isComment: Bool = false
        var cancelType: Bool = false
        var comment: String = ""
        
        let today = Date() //현재 시각 구하기
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var dateString = dateFormatter.string(from: today)
        
//        if self.datas["formType"]["requestCode"].string == "04" {
        if self.datas["formType"]["formCode"] != nil {
            if self.datas["formType"]["formCode"].string == "03" {
                isComment = true
            } else {
                isComment = false
            }
        } else {
            isComment = false
        }
        
        if self.datas["formType"]["cancelType"] != nil {
            if self.datas["formType"]["cancelType"].string == "Y" {
                cancelType = true
            } else {
                cancelType = false
            }
        } else {
            cancelType = false
        }
        
        if self.datas["result"]["docjdgmtcont"] != nil {
            comment = self.datas["result"]["docjdgmtcont"].string!
        }
        
        if self.datas["result"]["docjdgmtday"] != nil {
            dateString = self.datas["result"]["docjdgmtday"].string!
        }
        
        
        
        
        let signAlert = SignAlert(isComment: isComment, name: titleLabel, cancelType: cancelType, strComment: comment, strDate: dateString)
        signAlert.setSignAlertAction(signAlertAction: self)
        
        if self.datas["result"]["regday"] != nil {
            signAlert.setRegDay(regday: self.datas["result"]["regday"].string!)
        }
        
        signAlert.setSignFormSN(signFormSN: self.datas["formType"]["signFormSN"].string!)
        
        
        signAlert.show(animated: true)
    }
}
