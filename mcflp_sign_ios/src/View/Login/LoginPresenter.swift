//
//  LoginPresenter.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 1..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import Firebase

class LoginPresenter {
    private var net: Net!
    private var loginMvpProtocol: LoginMvpProtocol!
    
    init(loginMvpProtocol: LoginMvpProtocol) {
        self.net = Net()
        self.loginMvpProtocol = loginMvpProtocol
    }
    
    public func apiAddDeviceInfo() {
//        let token = Messaging.messaging().fcmToken
//        UserDefaults.standard.set(token, forKey: "fcmToken")
//
        let SID: String = (UIDevice.current.identifierForVendor?.uuidString)!

//        let RegID: String = UserDefaults.standard.string(forKey: "fcmToken")!

        net.request(url: ApiDefine.addDeviceInfo,
                    params: net.getAddDeviceParams(deviceSID: SID, deviceRegID: "", deviceType: "1")) { (data) in
                        print("data: \(data)")
                        UserDefaults.standard.set(true, forKey: "isRecentDate")
                        self.loginMvpProtocol.apiAddDeviceInfo()
        }
    }
    
    public func apiAutoLogin() {
        
        let SID: String = (UIDevice.current.identifierForVendor?.uuidString)!
        let RegID: String = UserDefaults.standard.string(forKey: "fcmToken")!
        
        net.request(url: ApiDefine.autoLogin,
                    params: net.getAutoLoginParams(token: UserDefaults.standard.string(forKey: "token")!, userSN: UserDefaults.standard.string(forKey: "userSN")!, deviceSID: SID, deviceRegID: RegID)) { (data) in
                        print("data: \(data)")
                        SaveDatas.LoginInfo = data["result"]
                        
                        
                        
                        UserDefaults.standard.set(data["token"].string, forKey: "token")
                        self.loginMvpProtocol.apiAutoLogin()
                        
        }
    }
}
