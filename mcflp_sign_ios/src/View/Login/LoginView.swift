//
//  LoginView.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 1..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import UIKit
import SlideMenuControllerSwift
import Alamofire
import SwiftyJSON
import FileBrowser

class LoginView: UIViewController, LoginMvpProtocol {
    private var loginPresenter: LoginPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginPresenter = LoginPresenter(loginMvpProtocol: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if UserDefaults.standard.bool(forKey: "isRecentDate") {
            let certificationView = CertificationView()
            self.present(certificationView, animated: false, completion: nil)
        } else {
            loginPresenter.apiAddDeviceInfo()
        }
    }
    
    func apiAddDeviceInfo() {
        let certificationView = CertificationView()
        self.present(certificationView, animated: false, completion: nil)
    }
    
    func apiAutoLogin() {
        let mainView = MainView()
//        mainView.setFormOpen(openSignFormSN: "close")
        let leftMenuView = LeftMenuView()
        CertificationView.slideMenuController = SlideMenuController(mainViewController: mainView, leftMenuViewController: leftMenuView)
        
        self.present(CertificationView.slideMenuController, animated: false, completion: nil)
    }
}
