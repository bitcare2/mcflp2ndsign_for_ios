//
//  LoginMvpProtocol.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 1..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation

@objc protocol LoginMvpProtocol {
    func apiAddDeviceInfo()
    func apiAutoLogin()
}
