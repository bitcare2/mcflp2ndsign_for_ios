//
//  CertificationPresenter.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 1..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import SwiftyJSON

extension String.CharacterView {
    /// This method makes it easier extract a substring by character index where a character is viewed as a human-readable character (grapheme cluster).
    internal func substring(start: Int, offsetBy: Int) -> String? {
        guard let substringStartIndex = self.index(startIndex, offsetBy: start, limitedBy: endIndex) else {
            return nil
        }
        
        guard let substringEndIndex = self.index(startIndex, offsetBy: start + offsetBy, limitedBy: endIndex) else {
            return nil
        }
        
        return String(self[substringStartIndex ..< substringEndIndex])
    }
}

class CertificationPresenter {
    private var net: Net!
    private var certificationMvpProtocol: CertificationMvpProtocol!
    
    init(certificationMvpProtocol: CertificationMvpProtocol) {
        self.net = Net()
        self.certificationMvpProtocol = certificationMvpProtocol
    }
    
    private func format(phoneNumber sourcePhoneNumber: String) -> String? {
        
        // Remove any character that is not a number
        let numbersOnly = sourcePhoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let length = numbersOnly.characters.count
        let hasLeadingOne = numbersOnly.hasPrefix("1")
        
        // Check for supported phone number length
//        guard length == 7 || length == 10 || (length == 11 && hasLeadingOne) else {
//            return nil
//        }
        
        let hasAreaCode = (length >= 11)
        var sourceIndex = 0
        
        // Leading 1
        var leadingOne = ""
        if hasLeadingOne {
            leadingOne = "1 "
            sourceIndex += 1
        }
        
        // Area code
        var areaCode = ""
        let areaCodeLength = 3
        guard let areaCodeSubstring = numbersOnly.characters.substring(start: sourceIndex, offsetBy: areaCodeLength) else {
            return nil
        }
        sourceIndex += areaCodeLength
        
        // Prefix, 3 characters
        let prefixLength = 4
        guard let prefix = numbersOnly.characters.substring(start: sourceIndex, offsetBy: prefixLength) else {
            return nil
        }
        sourceIndex += prefixLength
        
        // Suffix, 4 characters
        let suffixLength = 4
        guard let suffix = numbersOnly.characters.substring(start: sourceIndex, offsetBy: suffixLength) else {
            return nil
        }
        
        return areaCodeSubstring + "-" + prefix + "-" + suffix
    }
    
    public func apiCheckLoginKey(number: String) {
        let SID: String = (UIDevice.current.identifierForVendor?.uuidString)!
        let RegID: String = UserDefaults.standard.string(forKey: "fcmToken")!
        
        var loginType: String = "03"
        
        if number.first == "C" {
            loginType = "03"
        } else if number.first == "S" {
            loginType = "04"
        } else if number.first == "J" {
            loginType = "05"
        } else if number.first == "J" {
            loginType = "05"
        } else if number.first == "A" {
            loginType = "03"
        }
        
        print("loginType: \(loginType)")
        
        net.request(url: ApiDefine.checkLoginkey,
                    params: net.getCheckLoginKeyParams(
                        deviceSID: SID, deviceRegID: RegID, deviceType: "1", loginType: loginType, loginKey: number)) { (data) in
                            print("data: \(data)")
                            
                            if data["serviceCode"] == 100 {
                                SaveDatas.UserInfo = data["result"]
                                self.certificationMvpProtocol.apiCheckLoginKey(loginType: loginType)
                            } else {
                                self.certificationMvpProtocol.apiError(msg: data["serviceMsg"].string!)
                            }
        }
    }
    
    public func apiLogin(loginType: String, number: String) {
        let SID: String = (UIDevice.current.identifierForVendor?.uuidString)!
        let RegID: String = UserDefaults.standard.string(forKey: "fcmToken")!
        
        let userInfo: JSON = SaveDatas.UserInfo
        
        let numbersOnly = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let length = numbersOnly.characters.count
        
        
        if (loginType == "03" || loginType == "08") && length < 11 {
            self.certificationMvpProtocol.apiError(msg: "올바른 휴대폰 번호를 입력해 주세요")
        } else {
            var numberFormat: String = number
            
            if (loginType == "03" || loginType == "08") {
                numberFormat = self.format(phoneNumber: number)!
            }
            
            print("numberFormat: \(numberFormat)")
            
            var userSN: String = ""
            
            if userInfo["userSN"].string != nil {
                userSN = userInfo["userSN"].string!
            }
            
            
            net.request(url: ApiDefine.login,
                        params: net.getLoginParams(
                            loginType: loginType, appDeviceSN: userInfo["appDeviceSN"].string!, loginSN: userInfo["loginSN"].string!,
                            deviceSID: SID, deviceRegID: RegID, userSN: userSN, number: numberFormat)) { (data) in
                                print("data: \(data)")
                                if data["serviceCode"] == 100 {
                                    SaveDatas.LoginInfo = data["result"]
                                    
                                    UserDefaults.standard.set(data["token"].string, forKey: "token")
                                    
                                    
                                    if data["result"]["userSN"] != nil { //비회원이 아닐 때
                                        UserDefaults.standard.set(data["result"]["userSN"].string!, forKey: "userSN")
                                        
                                        if data["result"]["signFormSN"] != nil {
                                            self.getFormInfo(signFormSN: data["result"]["signFormSN"].string!, isNoUser: false)
                                        } else {
                                            self.certificationMvpProtocol.apiLogin()
                                        }
                                    } else { //비회원 일 때
                                        self.getFormInfo(signFormSN: data["result"]["signFormSN"].string!, isNoUser: true)
                                    }
                                    
                                } else {
                                    self.certificationMvpProtocol.apiError(msg: data["serviceMsg"].string!)
                                }
                                                    
                                                    
            }
        }
    }
    
    public func getFormInfo(signFormSN: String, isNoUser: Bool) {
        print("call getFormInfo")
        
        net.request(url: ApiDefine.getFormInfo,
                    params: net.getFormInfoParams(token: UserDefaults.standard.string(forKey: "token")!, signFormSN: signFormSN)) { (data) in
                        print("data: \(data)")
                        
                        UserDefaults.standard.set(data["token"].string, forKey: "token")
                        
                        print("data: \(data["result"])")
                        
                        self.certificationMvpProtocol.getFormInfo(datas: data, isNoUser: isNoUser)
//                        self.mainMvpProtocol.getFormInfo(datas: data, index: 0)
        }
    }
}
