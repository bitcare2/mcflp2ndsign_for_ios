//
//  CertificationMvpView.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 1..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol CertificationMvpProtocol {
    func apiCheckLoginKey(loginType: String)
    func apiLogin()
    func apiError(msg: String)
    func getFormInfo(datas: JSON, isNoUser: Bool)
}
