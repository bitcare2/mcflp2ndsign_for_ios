//
//  CertificationView.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 1..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import UIKit
import SlideMenuControllerSwift
import Alamofire
import SwiftyJSON
import TextAttributes

class CertificationView: UIViewController, CertificationMvpProtocol {
    
    public static var slideMenuController: SlideMenuController!
    
    private var certificationPresenter: CertificationPresenter!
    
    private var bgTitle = UILabel()
    private var bgContent = UILabel()
    private var number = ImageTextField()
    private var confirm = UIButton()
    
    private var mPhase: Int = 0
    
    private var loginType: String!
    
    override func viewDidLoad() {
        certificationPresenter = CertificationPresenter(certificationMvpProtocol: self)
        setStatusBar()
        setTitlebar()
        setBackground()
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func apiCheckLoginKey(loginType: String) {
        self.loginType = loginType
        
        self.mPhase = 1
        self.setPhase(phase: 1)
        self.number.text = ""
        
        if loginType == "03" {
            bgContent.text = CertificationPhase2Strings.contentPhone
        } else if loginType == "04" {
            bgContent.text = CertificationPhase2Strings.contentDoc
        } else if loginType == "05" {
            bgContent.text = CertificationPhase2Strings.contentCode
        } else if loginType == "08" {
            bgContent.text = CertificationPhase2Strings.contentPhone
        }
        
        self.view.endEditing(true)
//        self.number.autocapitalizationType = .none
//        self.number.keyboardType = .numberPad

    }
    
    func apiLogin() {
        let mainView = MainView()
//        mainView.setFormOpen(openSignFormSN: openSignFormSN)
        let leftMenuView = LeftMenuView()
        
        CertificationView.slideMenuController = SlideMenuController(mainViewController: mainView, leftMenuViewController: leftMenuView)
        
        self.present(CertificationView.slideMenuController, animated: false, completion: nil)
    }
    
     func getFormInfo(datas: JSON, isNoUser: Bool) {
        if datas == nil || datas["result"].isEmpty || datas["userInfo"].isEmpty || datas["formType"].isEmpty {
//            let dialog = UIAlertController(title: "error", message: "상세정보가 없습니다.", preferredStyle: .alert)
//            let action = UIAlertAction(title: "확인", style: .default)
//
//            dialog.addAction(action)
//
//            self.present(dialog, animated: true, completion: nil)
            
            let mainView = MainView()
            let leftMenuView = LeftMenuView()
            
            CertificationView.slideMenuController = SlideMenuController(mainViewController: mainView, leftMenuViewController: leftMenuView)
            
            self.present(CertificationView.slideMenuController, animated: false, completion: nil)
        } else {
            self.mPhase = 0
            self.setPhase(phase: 0)
            self.number.text = ""
            self.bgContent.text = CertificationPhase1Strings.content
            
            
            let ozview = OZView()
            if isNoUser {
                ozview.setViewType(type: OZView.VIEW_TYPE_LOGINNOUSER)
            } else {
                ozview.setViewType(type: OZView.VIEW_TYPE_LOGINUSER)
            }
            ozview.setData(datas: datas)
            self.present(ozview, animated: false, completion: nil)
            
            
        }
    }
    
    func apiError(msg: String) {
        let dialog = UIAlertController(title: "error", message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "확인", style: .default)
        
        dialog.addAction(action)
        
        self.present(dialog, animated: true, completion: nil)
    }
    
    private func setStatusBar() {
        self.view.frame = ((UIApplication.shared.delegate as! AppDelegate).window?.frame)!
        self.view.backgroundColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = true
        
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = CertificationColors.statusBarColor
        statusBarView.backgroundColor = statusBarColor
        self.view.addSubview(statusBarView)
    }
    
    private func setTitlebar() {
//        let bar = CertificationImages.bar
        let bar = UIView()
        bar.backgroundColor = HexColor.hexStringToUIColor(hex: "#2967C2")
        bar.frame = CertificationFrames.bar
        self.view.addSubview(bar)
        
        let logo = CertificationImages.barlogo
        logo.frame = CertificationFrames.titleLogo
        
        self.view.addSubview(logo)
        
        let titleLabel = UILabel()
        titleLabel.frame = CertificationFrames.titleLabel
        titleLabel.text = CertificationStrings.navigationTitle
        titleLabel.textColor = CertificationColors.titleLabel
        titleLabel.font = CertificationFonts.titleLabel
        titleLabel.adjustsFontSizeToFitWidth = true
        
        self.view.addSubview(titleLabel)
        
        let emblem = UIButton()
        emblem.frame = CGRect(x: ScreenInfo.currentDeviceWidth - ScreenInfo.getWidth(width: 95),
                              y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 15),
                              width: ScreenInfo.getWidth(width: 80),
                              height: ScreenInfo.getHeight(height: 30))
        emblem.layer.cornerRadius = ScreenInfo.getWidth(width: 15)
        emblem.setTitle("서명전용", for: .normal)
        emblem.setTitleColor(HexColor.hexStringToUIColor(hex: "#4380DB"), for: .normal)
        emblem.titleLabel?.font =  UIFont(name: "NanumGothicBold", size: ScreenInfo.getHeight(height: 14))
        emblem.backgroundColor = HexColor.hexStringToUIColor(hex: "#FFFFFF")
        
        self.view.addSubview(emblem)
        
    }
    
    private func setBackground() {
        let statusBarRect = UIApplication.shared.statusBarFrame
        let statusBarHeight = statusBarRect.height
        
        //background
        let bgView = CertificationImages.bgView
        bgView.frame = CertificationFrames.bgView
        
        self.view.addSubview(bgView)
        
        //login rect background
        let whiteBG = UIView()
        whiteBG.frame = CertificationFrames.whiteBG
        whiteBG.backgroundColor = .white
        whiteBG.layer.cornerRadius = 10
        
        self.view.addSubview(whiteBG)
        
        //bg title
        bgTitle.frame = CertificationFrames.bgTitle
        bgTitle.text = CertificationPhase1Strings.title
        bgTitle.textColor = CertificationColors.bgTitle
        bgTitle.font = CertificationFonts.bgTitle
        
        self.view.addSubview(bgTitle)
        
        //bg content
        bgContent.frame = CertificationFrames.bgContent
        bgContent.text = CertificationPhase1Strings.content
        bgContent.textColor = CertificationColors.bgContent
        bgContent.font = CertificationFonts.bgContent
        
        self.view.addSubview(bgContent)
        
        number.frame = CertificationFrames.number
        number.leftViewMode = UITextFieldViewMode.always
        number.leftView = UIView(frame: CertificationFrames.number_leftView)
        number.layer.borderWidth = ScreenInfo.getWidth(width: 1)
        number.layer.borderColor = CertificationColors.numberBorder.cgColor
        number.layer.cornerRadius = ScreenInfo.getWidth(width: 5)
        number.backgroundColor = CertificationColors.number
        number.placeholder = CertificationPhase1Strings.placeholder
        number.autocapitalizationType = .allCharacters
        
        self.view.addSubview(number)
        
        
        confirm.frame = CertificationFrames.confirm
//        confirm.setBackgroundImage(CertificationImages.confirm, for: UIControlState.normal)
        confirm.backgroundColor = HexColor.hexStringToUIColor(hex: "#2967C2")
        confirm.layer.borderWidth = ScreenInfo.getWidth(width: 1)
        confirm.layer.borderColor = HexColor.hexStringToUIColor(hex: "#2967C2").cgColor
        confirm.layer.cornerRadius = ScreenInfo.getWidth(width: 5)
        confirm.setTitle(CertificationPhase1Strings.button, for: .normal)
        confirm.setTitleColor(.white, for: .normal)
        confirm.titleLabel?.font = CertificationFonts.confirm
        confirm.addTarget(self, action: #selector(CertificationView.confirmButtonClick(_sender:)), for: .touchUpInside)
        
        self.view.addSubview(confirm)
        
        //support
        let supportTxt = UILabel()
        supportTxt.frame = CertificationFrames.supportTxt
        supportTxt.text = CertificationStrings.inquiry
        supportTxt.textColor = CertificationColors.supportTxt
        supportTxt.font = CertificationFonts.supportTxt
        
        self.view.addSubview(supportTxt)
        
        
        //logo
        let logo = CertificationImages.logo
        logo.frame = CertificationFrames.logo
        
        self.view.addSubview(logo)
        
        //logo title
        let logoTitle = UILabel()
        logoTitle.frame = CertificationFrames.logoTitle
        logoTitle.text = CertificationStrings.bottom
        logoTitle.textColor = CertificationColors.logoTitle
        logoTitle.font = CertificationFonts.LogoTitle
        
        self.view.addSubview(logoTitle)
    }
    
    private func setPhase(phase: Int) {
        if phase == 0 {
            bgTitle.text = CertificationPhase1Strings.title
            bgContent.text = CertificationPhase1Strings.content
            number.placeholder = CertificationPhase1Strings.placeholder
            number.autocapitalizationType = .allCharacters
            number.keyboardType = .default
            confirm.setTitle(CertificationPhase1Strings.button, for: .normal)
        } else if phase == 1 {
            bgTitle.text = CertificationPhase2Strings.title
            number.placeholder = CertificationPhase2Strings.placeholder
            number.autocapitalizationType = .none
            number.keyboardType = .numberPad
            confirm.setTitle(CertificationPhase2Strings.button, for: .normal)
        }
    }
    
    @objc public func confirmButtonClick(_sender: UIButton) {
        if mPhase == 0 {
            self.certificationPresenter.apiCheckLoginKey(number: self.number.text!)
        } else if mPhase == 1 {
            self.certificationPresenter.apiLogin(loginType: self.loginType, number: self.number.text!)
        }
        
    }
}
