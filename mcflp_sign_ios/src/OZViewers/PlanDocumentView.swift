//
//  PlanDocumentView.swift
//  MCFLP
//
//  Created by acid on 2018. 2. 21..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import UIKit
import Material
import TextAttributes
import SwiftyJSON
//import ObjectMapper
import Material
//import NumericKeyboard
//import KRProgressHUD
//import KRActivityIndicatorView
import Alamofire

class PlanDocumentView: UIViewController, OZReportCommandListener, TextFieldDelegate {
    
    /// 오즈 뷰어
    private var viewer: OZReportViewer!
    /// 뷰어에 전달할 데이터
    public var data: [String: Any?] = [:]
    /// 문서 상태 코드
    public var lstplanstatcode: String = "01"
    
    /// 신규작성여부
    fileprivate var isFirst: Bool = true
    /// 등록여부
    fileprivate var isRegist: Bool = false
    fileprivate var lstplantempsn: String = ""
    
    fileprivate var target: String = ""
    fileprivate var max: Int = 0
    fileprivate var isPhone: String = ""
    @objc func createView(){
        
    
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        bClose.isEnabled = false
//        bButton2.isEnabled = false
//        bButton1.isEnabled = false
//        perform(#selector(self.createView), with:nil, afterDelay: 0.1)
        
        let reportView = UIView()
        reportView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        self.view.addSubview(reportView)
        
        let strParam = """
        connection.servlet=http://59.10.164.94:8080/oz70/server
        connection.reportname=/realused/plan.ozr
        viewer.zoombydoubletap=false
        viewer.minzoom=10
        viewer.usetoolbar=false
        viewer.progresscommand=true
        eform.signpad_type=zoom
        connection.pcount=1
        connection.args1=jsonData=\(JSON(data).rawString([writingOptionsKeys.castNilToNSNull : true])!)
        """
        
        let reportViewer: OZReportViewer? = OZReportAPI.createViewer(self, view: reportView, listener:self, param:strParam, delimiter:"\n", closeButton: "" )
        viewer = reportViewer
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    /// 뷰가 나타난 이후
    /// 통신은 여기에서함.
    /// - Parameter animated:
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    /// 텍스트 필드 MaxLength
    ///
    /// - Parameters:
    ///   - textField:
    ///   - text:
    func textField(textField: UITextField, didChange text: String?) {
        var limitLength: Int = max
        let currentText = textField.text ?? ""
//        if currentText.length >= limitLength {
//            if currentText.length > limitLength {
//                textField.deleteBackward()
//            }
//        }
    }
    /// Text Watcher
    ///
    /// - Parameter textField:
    @objc fileprivate func textFieldDidChange(textField: UITextField){
        
        viewer.document_TriggerExternalEvent("number", param2: textField.text, param3: target, param4: "\(isPhone)")
    }
    
    //여기서부터 리스너
    func ozCloseCommand() {
        
    }
    func ozExitCommand() {
        
    }
    func ozErrorCommand(_ code: String!, _ message: String!, _ detailmessage: String!, _ reportname: String!) {
        
    }
    func ozUserActionCommand(_ type: String!, _ attr: String!) {
//        Logger.info(message: type + " : " + attr)
    }
    func ozExportCommand(_ code: String!, _ path: String!, _ filename: String!, _ pagecount: String!, _ filepaths: String!) {
        
    }
    func ozeFormInputEventCommand(_ docindex: String!, _ formid: String!, _ eventname: String!, _ mainscreen: String!) {
        
    }
    func ozPageChangeCommand(_ docindex: String!) {
        
    }
    func ozReportChangeCommand(_ docindex: String!) {
        
    }
    func ozProgressCommand(_ step: String!, _ state: String!, _ reportname: String!) {
        if step == "4" && state == "2" {
//            bClose.isEnabled = true
//            bButton2.isEnabled = true
//            bButton1.isEnabled = true
        }
    }
    func ozExportMemoryStreamCallBack(_ outputdata: String!) {
        
    }
    
    func ozUserEvent(_ param1: String!, _ param2: String!, _ param3: String!) -> String! {
//        Logger.info(message: "param1 : "+param1)
//        Logger.info(message: "param2 : "+param2)
//        Logger.info(message: "param3 : "+param3)
        
//        if(param1=="address"){
//            let addressInputView = AddressInputView()
//            addressInputView.showPopup(vparent, complite: { (address1, address2, bjdcode, zipcode) in
//                self.viewer.document_TriggerExternalEvent(zipcode!, param2: address1!, param3: address2!, param4: bjdcode!)
//            })
//        } else if(param1=="error"){
//            CAlert().showPopup(vparent ,title: "작성 사항 확인", content: param2, closeButton: "확인")
//        } else if(param1=="number"){
//            Logger.info(message: "param2 : "+param2)
//            Logger.info(message: "param3 : "+param3)
//            self.tfNumber.text = param2
//            self.tfNumber.becomeFirstResponder()
//            self.tfNumber.delegate = self
//            self.tfNumber.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
//            self.target = "\(param3.split(separator: ":")[0])"
//            self.max = Int("\(param3.split(separator: ":")[1])")!
//            self.isPhone = "\(param3.split(separator: ":")[2])"
//            return "1"
//        } else if(param1=="endnumber"){
//            Logger.info(message: "param2 : "+param2)
//            Logger.info(message: "param3 : "+param3)
//            self.tfNumber.text = ""
//            self.tfNumber.resignFirstResponder()
//            return "1"
//        } else if(param1 == "temp") {
//            KRProgressHUD.show()
//            var parameter: [String: Any]? = CommonUtil.convertToDictionary(text: param2)
//            parameter?.updateValue(lstplanstatcode, forKey: "lstplanstatcode")
//            parameter?.updateValue("N", forKey: "regcomtyn")
//            parameter?.updateValue(CommonUtil.convertToDictionary(text: param2)!["confrmethcode"] as! String == "04" ? "N" : "Y", forKey: "selfyn")
//            parameter?.updateValue(CommonUtil.convertToDictionary(text: param2)!["docusersn"] as! String, forKey: "usersn")
//            let data: PlanSaveVO = Mapper<PlanSaveVO>().map(JSON: parameter!)!
//            if isFirst {
//                callTempSavelstplan(parameter: data)
//            } else {
//                callTempModifylstplan(parameter: data)
//            }
//        } else if(param1 == "done") {
//            KRProgressHUD.show()
//            var parameter: [String: Any]? = CommonUtil.convertToDictionary(text: param2)
//            parameter?.updateValue(lstplanstatcode, forKey: "lstplanstatcode")
//            parameter?.updateValue("Y", forKey: "regcomtyn")
//            parameter?.updateValue(CommonUtil.convertToDictionary(text: param2)!["confrmethcode"] as! String == "04" ? "N" : "Y", forKey: "selfyn")
//            parameter?.updateValue(CommonUtil.convertToDictionary(text: param2)!["docusersn"] as! String, forKey: "usersn")
//            lstplantempsn = parameter!["lstplantempsn"] as! String
//            isRegist = true
//            let data: PlanSaveVO = Mapper<PlanSaveVO>().map(JSON: parameter!)!
//            if isFirst {
//                callTempSavelstplan(parameter: data)
//            } else {
//                callTempModifylstplan(parameter: data)
//            }
//        }
        return "";
    }
    
    /// 임시저장된 것을 작성완료로 변경후 본테이블로 카피
//    fileprivate func callCopyFromlstplantempTolstplan(){
//        isRegist = false
//        KRProgressHUD.show()
//        let api = NetworkManager(path: ApiDefine.server.testServer
//                                        + ApiDefine.lstplan.copyFromLstplanTempToLstplan
//                                        + "?token="+(CommonShared.getSharedForString(key: ShareDefine.MCFLP_USER_TOKEN) ?? "")
//                                        + "&lstplantempsn=\(lstplantempsn)&lstplanstatcode=\(lstplanstatcode)",
//                                 parameters: [:], method: .post, isParamInclude: false, isHeaders: false)
//        api.request(success: resultToJsonCopyFromlstplantempTolstplan(_:)) { (error) in
//            Logger.error(message: "\(error.debugDescription)")
//            KRProgressHUD.dismiss {
//                CAlert().showPopup(DoctorMainView.sharedInstance, title: "네트워크 오류", content: "잠시후 다시 시도해 주세요.", closeButton: "확인")
//            }
//        }
//    }
    
    /// 임시저장된 것을 작성완료로 변경후 본테이블로 카피 후 작동
    ///
    /// - Parameter data:
//    fileprivate func resultToJsonCopyFromlstplantempTolstplan(_ data: JSON){
//        KRProgressHUD.dismiss {
//            let result = Mapper<NetworkVO>().map(JSON: data.dictionaryObject!)
//            if result!.serviceCode == "100" {
//                let json = JSON(result?.result! ?? "")
//                CommonShared.setSharedForString(key: ShareDefine.MCFLP_USER_TOKEN, value: json["token"].stringValue)
//                super.clickButton(sender: self.bClose)
//                CAlert().showPopup(DoctorMainView.sharedInstance, title: "등록", content: "등록 되었습니다.", closeButton: "확인")
//            }else{
//                Logger.info(message: "실패")
//                if result?.result != nil {
//                    CAlert().showPopup(DoctorMainView.sharedInstance, title: "네트워크 오류", content: "잠시후 다시 시도해 주세요.", closeButton: "확인")
//                    let json = JSON(result?.result! ?? "")
//                    CommonShared.setSharedForString(key: ShareDefine.MCFLP_USER_TOKEN, value: json["token"].stringValue)
//                } else {
//                    CAlert().showPopup(DoctorMainView.sharedInstance, title: "로그아웃", content: "로그인이 만료되었습니다.", closeButton: "확인")
//                    DoctorMainView.sharedInstance?.onClickLogout(sender: nil)
//                }
//            }
//        }
//    }
    
    /// 임시저장 수정
    ///
    /// - Parameter parameter: 데이터
//    fileprivate func callTempModifylstplan(parameter: PlanSaveVO){
//
//        let param: NSDictionary = parameter.toJSON() as NSDictionary
//        let api = NetworkManager(path: ApiDefine.server.testServer + ApiDefine.lstplan.modify.lstplanTempInfo + "?token="+(CommonShared.getSharedForString(key: ShareDefine.MCFLP_USER_TOKEN) ?? ""),
//                                 parameters: param, method: .put, isParamInclude: false, isHeaders: false)
//        api.request(success: resultToJsonTemplstplan(_:)) { (error) in
//            Logger.error(message: "\(error.debugDescription)")
//            KRProgressHUD.dismiss {
//                CAlert().showPopup(DoctorMainView.sharedInstance, title: "네트워크 오류", content: "잠시후 다시 시도해 주세요.", closeButton: "확인")
//            }
//        }
//    }
    
    /// 임시저장
    ///
    /// - Parameter parameter: 데이터
//    fileprivate func callTempSavelstplan(parameter: PlanSaveVO){
//
//        let param: NSDictionary = parameter.toJSON() as NSDictionary
//        let api = NetworkManager(path: ApiDefine.server.testServer + ApiDefine.lstplan.save.lstplanTempInfo + "?token="+(CommonShared.getSharedForString(key: ShareDefine.MCFLP_USER_TOKEN) ?? ""),
//                                 parameters: param, method: .post, isParamInclude: false, isHeaders: false)
//        api.request(success: resultToJsonTemplstplan(_:)) { (error) in
//            Logger.error(message: "\(error.debugDescription)")
//            KRProgressHUD.dismiss {
//                CAlert().showPopup(DoctorMainView.sharedInstance, title: "네트워크 오류", content: "잠시후 다시 시도해 주세요.", closeButton: "확인")
//            }
//        }
//    }
    
    /// 임시저장 결과
    ///
    /// - Parameter data:
//    fileprivate func resultToJsonTemplstplan(_ data: JSON){
//        KRProgressHUD.dismiss {
//            let result = Mapper<NetworkVO>().map(JSON: data.dictionaryObject!)
//            if result!.serviceCode == "100" {
//                let json = JSON(result?.result! ?? "")
//                CommonShared.setSharedForString(key: ShareDefine.MCFLP_USER_TOKEN, value: json["token"].stringValue)
//                self.isFirst = false
//                if self.isRegist {
//                    self.callCopyFromlstplantempTolstplan()
//                } else {
//                    CAlert().showPopup(DoctorMainView.sharedInstance, title: "임시저장", content: "임시저장 되었습니다.", closeButton: "확인")
//                }
//            }else{
//                Logger.info(message: "실패")
//                if result?.result != nil {
//                    CAlert().showPopup(DoctorMainView.sharedInstance, title: "네트워크 오류", content: "잠시후 다시 시도해 주세요.", closeButton: "확인")
//                    let json = JSON(result?.result! ?? "")
//                    CommonShared.setSharedForString(key: ShareDefine.MCFLP_USER_TOKEN, value: json["token"].stringValue)
//                } else {
//                    CAlert().showPopup(DoctorMainView.sharedInstance, title: "로그아웃", content: "로그인이 만료되었습니다.", closeButton: "확인")
//                    DoctorMainView.sharedInstance?.onClickLogout(sender: nil)
//                }
//            }
//        }
//    }
    
}
