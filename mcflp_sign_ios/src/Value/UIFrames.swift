//
//  UIFrame.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 2..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import SlideMenuControllerSwift

struct LoginViewFrames {
    static let bgView: CGRect = CGRect(x:0,
                                       y:0,
                                       width: ScreenInfo.currentDeviceWidth,
                                       height: ScreenInfo.currentDeviceHeight)
    
    
    static let bottomLogo: CGRect = CGRect(x: ScreenInfo.getWidth(width:79),
                                     y:ScreenInfo.currentDeviceHeight - ScreenInfo.getHeight(height: 100),
                                     width: ScreenInfo.getWidth(width: 35),
                                     height: ScreenInfo.getHeight(height: 35))
    static let logoTitle: CGRect = CGRect(x: ScreenInfo.getWidth(width:124),
                                          y:ScreenInfo.currentDeviceHeight - ScreenInfo.getHeight(height: 96),
                                          width: ScreenInfo.getWidth(width: 200),
                                          height: ScreenInfo.getHeight(height: 30))
}

struct CertificationFrames {
    static let bar: CGRect = CGRect(x:0,
                                    y:UIApplication.shared.statusBarFrame.height,
                                    width: ScreenInfo.currentDeviceWidth,
                                    height: ScreenInfo.getHeight(height: 60))
    static let titleLogo: CGRect = CGRect(x: ScreenInfo.getWidth(width:15),
                                          y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 15),
                                          width: ScreenInfo.getWidth(width: 28),
                                          height: ScreenInfo.getHeight(height: 28))
    static let titleLabel: CGRect = CGRect(x: ScreenInfo.getWidth(width:48),
                                           y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 10),
                                           width: ScreenInfo.currentDeviceWidth - ScreenInfo.getWidth(width:48) - ScreenInfo.getWidth(width: 95),
                                           height: ScreenInfo.getHeight(height: 37))
    static let bgView: CGRect = CGRect(x:0,
                                       y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 60),
                                       width: ScreenInfo.currentDeviceWidth,
                                       height: ScreenInfo.currentDeviceHeight - ScreenInfo.getHeight(height: 60))
    static let whiteBG: CGRect = CGRect(x:ScreenInfo.getWidth(width: 25),
                                        y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 150),
                                        width: ScreenInfo.getWidth(width: 325),
                                        height: ScreenInfo.getHeight(height: 240))
    static let bgTitle: CGRect = CGRect(x: ScreenInfo.getWidth(width:40),
                                    y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 170),
                                    width: ScreenInfo.getWidth(width: 200),
                                    height: ScreenInfo.getHeight(height: 30))
    static let bgContent: CGRect = CGRect(x: ScreenInfo.getWidth(width:41),
                                         y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 210),
                                         width: ScreenInfo.getWidth(width: 300),
                                         height: ScreenInfo.getHeight(height: 30))
    static let number: CGRect = CGRect(x: ScreenInfo.getWidth(width: 40),
                                       y: UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 250),
                                       width: ScreenInfo.getWidth(width: 295),
                                       height: ScreenInfo.getHeight(height: 40))
    static let number_leftView: CGRect = CGRect(x: 0,
                                                y: 0,
                                                width: ScreenInfo.getWidth(width: 20),
                                                height: ScreenInfo.getHeight(height: 50))
    static let confirm: CGRect = CGRect(x: ScreenInfo.getWidth(width:40),
                                        y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 310),
                                        width: ScreenInfo.getWidth(width: 295),
                                        height: ScreenInfo.getHeight(height: 50))
    static let supportTxt: CGRect = CGRect(x: ScreenInfo.getWidth(width:45),
                                           y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 390),
                                           width: ScreenInfo.getWidth(width: 400),
                                           height: ScreenInfo.getHeight(height: 30))
    static let logo: CGRect = CGRect(x: ScreenInfo.getWidth(width:79),
                                     y:ScreenInfo.currentDeviceHeight - ScreenInfo.getHeight(height: 100),
                                     width: ScreenInfo.getWidth(width: 35),
                                     height: ScreenInfo.getHeight(height: 35))
    static let logoTitle: CGRect = CGRect(x: ScreenInfo.getWidth(width:124),
                                          y:ScreenInfo.currentDeviceHeight - ScreenInfo.getHeight(height: 96),
                                          width: ScreenInfo.getWidth(width: 200),
                                          height: ScreenInfo.getHeight(height: 30))
}

struct LeftMenuFrames {
    static let bar: CGRect = CGRect(x:0,
                                    y:0,
                                    width: SlideMenuOptions.leftViewWidth,
                                    height: ScreenInfo.getHeight(height: 120))
    static let logoBG: CGRect = CGRect(x:ScreenInfo.getWidth(width: 15),
                                    y:ScreenInfo.getHeight(height: 30),
                                    width: ScreenInfo.getWidth(width: 39),
                                    height: ScreenInfo.getHeight(height: 39))
    static let logo: CGRect = CGRect(x: ScreenInfo.getWidth(width: 20),
                                     y: ScreenInfo.getHeight(height: 35),
                                     width: ScreenInfo.getWidth(width: 28),
                                     height: ScreenInfo.getHeight(height: 28))
    static let name: CGRect = CGRect(x: ScreenInfo.getWidth(width: 13),
                                     y: ScreenInfo.getHeight(height: 68),
                                     width: ScreenInfo.getWidth(width: 100),
                                     height: ScreenInfo.getHeight(height: 30))
    static let hospital: CGRect = CGRect(x: ScreenInfo.getWidth(width: 13),
                                     y: ScreenInfo.getHeight(height: 85),
                                     width: ScreenInfo.getWidth(width: 200),
                                     height: ScreenInfo.getHeight(height: 30))
    static let menuWait: CGRect = CGRect(x: 0,
                                         y: ScreenInfo.getHeight(height: 120),
                                         width: SlideMenuOptions.leftViewWidth,
                                         height: ScreenInfo.getHeight(height: 60))
    static let menuWaitLabel: CGRect = CGRect(x: 20,
                                         y: ScreenInfo.getHeight(height: 120),
                                         width: SlideMenuOptions.leftViewWidth - ScreenInfo.getWidth(width: 20),
                                         height: ScreenInfo.getHeight(height: 60))
    static let menuComplete: CGRect = CGRect(x: 0,
                                         y: ScreenInfo.getHeight(height: 180),
                                         width: SlideMenuOptions.leftViewWidth,
                                         height: ScreenInfo.getHeight(height: 60))
    static let menuCompleteLabel: CGRect = CGRect(x: 20,
                                              y: ScreenInfo.getHeight(height: 180),
                                              width: SlideMenuOptions.leftViewWidth - ScreenInfo.getWidth(width: 20),
                                              height: ScreenInfo.getHeight(height: 60))
    static let menuInfo: CGRect = CGRect(x: 0,
                                         y: ScreenInfo.getHeight(height: 240),
                                         width: SlideMenuOptions.leftViewWidth,
                                         height: ScreenInfo.getHeight(height: 60))
    static let menuInfoLabel: CGRect = CGRect(x: 20,
                                              y: ScreenInfo.getHeight(height: 240),
                                              width: SlideMenuOptions.leftViewWidth - ScreenInfo.getWidth(width: 20),
                                              height: ScreenInfo.getHeight(height: 60))
    static let menuLogout: CGRect = CGRect(x: 0,
                                         y: ScreenInfo.getHeight(height: 300),
                                         width: SlideMenuOptions.leftViewWidth,
                                         height: ScreenInfo.getHeight(height: 60))
    static let menuLogoutLabel: CGRect = CGRect(x: 20,
                                                y: ScreenInfo.getHeight(height: 300),
                                                width: SlideMenuOptions.leftViewWidth - ScreenInfo.getWidth(width: 20),
                                                height: ScreenInfo.getHeight(height: 60))
}

struct MainFrames {
    static let bar: CGRect = CGRect(x:0,
                                    y:UIApplication.shared.statusBarFrame.height,
                                    width: ScreenInfo.currentDeviceWidth,
                                    height: ScreenInfo.getHeight(height: 60))
    static let menu: CGRect = CGRect(x: ScreenInfo.getWidth(width:15),
                                          y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 15),
                                          width: ScreenInfo.getWidth(width: 30),
                                          height: ScreenInfo.getHeight(height: 30))
    static let titleLabel: CGRect = CGRect(x: ScreenInfo.getWidth(width:70),
                                           y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 10),
                                           width: ScreenInfo.getWidth(width: 300),
                                           height: ScreenInfo.getHeight(height: 37))
    static let list: CGRect = CGRect(x:0,
                                    y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 60),
                                    width: ScreenInfo.currentDeviceWidth,
                                    height: ScreenInfo.currentDeviceHeight - (UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 60)))
    static let rightMenu: CGRect = CGRect(x: ScreenInfo.currentDeviceWidth - ScreenInfo.getWidth(width: 45),
                                          y: UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 15),
                                          width: ScreenInfo.getWidth(width: 30),
                                          height: ScreenInfo.getHeight(height: 30))
    static let hiddenrightMenu: CGRect = CGRect(x: ScreenInfo.currentDeviceWidth - ScreenInfo.getWidth(width: 60),
                                          y: UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 60),
                                          width: ScreenInfo.getWidth(width: 60),
                                          height: ScreenInfo.getHeight(height: 60))
}

struct MainListCellFrames {
    static let name: CGRect = CGRect(x: ScreenInfo.getWidth(width: 20),
                                     y: ScreenInfo.getHeight(height: 18) ,
                                     width: ScreenInfo.getWidth(width: 100),
                                     height: ScreenInfo.getHeight(height: 50))
    static let date: CGRect = CGRect(x: ScreenInfo.getWidth(width: 100),
                                     y: ScreenInfo.getHeight(height: 5) ,
                                     width: ScreenInfo.getWidth(width: 100),
                                     height: ScreenInfo.getHeight(height: 50))
    static let type: CGRect = CGRect(x: ScreenInfo.getWidth(width: 20),
                                     y: ScreenInfo.getHeight(height: 35) ,
                                     width: ScreenInfo.getWidth(width: 300),
                                     height: ScreenInfo.getHeight(height: 50))
    static let requestName1: CGRect = CGRect(x: ScreenInfo.getWidth(width: 20),
                                     y: ScreenInfo.getHeight(height: 82),
                                     width: ScreenInfo.getWidth(width: 127),
                                     height: ScreenInfo.getHeight(height: 30))
    static let requestName2: CGRect = CGRect(x: ScreenInfo.getWidth(width: 20),
                                      y: ScreenInfo.getHeight(height: 82) ,
                                      width: ScreenInfo.getWidth(width: 112),
                                      height: ScreenInfo.getHeight(height: 30))
    static let requestName3: CGRect = CGRect(x: ScreenInfo.getWidth(width: 20),
                                      y: ScreenInfo.getHeight(height: 82) ,
                                      width: ScreenInfo.getWidth(width: 152),
                                      height: ScreenInfo.getHeight(height: 30))
    static let requestName4: CGRect = CGRect(x: ScreenInfo.getWidth(width: 20),
                                      y: ScreenInfo.getHeight(height: 82) ,
                                      width: ScreenInfo.getWidth(width: 127),
                                      height: ScreenInfo.getHeight(height: 30))
    static let requestName5: CGRect = CGRect(x: ScreenInfo.getWidth(width: 20),
                                             y: ScreenInfo.getHeight(height: 82) ,
                                             width: ScreenInfo.getWidth(width: 162),
                                             height: ScreenInfo.getHeight(height: 30))
    static let number1: CGRect = CGRect(x: ScreenInfo.getWidth(width: 157),
                                      y: ScreenInfo.getHeight(height: 72) ,
                                      width: ScreenInfo.getWidth(width: 200),
                                      height: ScreenInfo.getHeight(height: 50))
    static let number2: CGRect = CGRect(x: ScreenInfo.getWidth(width: 142),
                                       y: ScreenInfo.getHeight(height: 72) ,
                                       width: ScreenInfo.getWidth(width: 200),
                                       height: ScreenInfo.getHeight(height: 50))
    static let number3: CGRect = CGRect(x: ScreenInfo.getWidth(width: 182),
                                       y: ScreenInfo.getHeight(height: 72) ,
                                       width: ScreenInfo.getWidth(width: 200),
                                       height: ScreenInfo.getHeight(height: 50))
    static let number4: CGRect = CGRect(x: ScreenInfo.getWidth(width: 157),
                                       y: ScreenInfo.getHeight(height: 72) ,
                                       width    : ScreenInfo.getWidth(width: 200),
                                       height: ScreenInfo.getHeight(height: 50))
    static let number5: CGRect = CGRect(x: ScreenInfo.getWidth(width: 192),
                                        y: ScreenInfo.getHeight(height: 72) ,
                                        width: ScreenInfo.getWidth(width: 200),
                                        height: ScreenInfo.getHeight(height: 50))
}

struct OZViewFrames {
    static let bar: CGRect = CGRect(x:0,
                                    y:UIApplication.shared.statusBarFrame.height,
                                    width: ScreenInfo.currentDeviceWidth,
                                    height: ScreenInfo.getHeight(height: 60))
    static let back: CGRect = CGRect(x: ScreenInfo.getWidth(width:15),
                                     y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 15),
                                     width: ScreenInfo.getWidth(width: 28),
                                     height: ScreenInfo.getHeight(height: 28))
    static let titleLabel: CGRect = CGRect(x: ScreenInfo.getWidth(width:60),
                                           y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 10),
                                           width: ScreenInfo.getWidth(width: 300),
                                           height: ScreenInfo.getHeight(height: 37))
    static let sign: CGRect = CGRect(x: ScreenInfo.getWidth(width:270),
                                     y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 15),
                                     width: ScreenInfo.getWidth(width: 100),
                                     height: ScreenInfo.getHeight(height: 28))
    static let ozview: CGRect = CGRect(x:0,
                                    y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 60),
                                    width: ScreenInfo.currentDeviceWidth,
                                    height: ScreenInfo.currentDeviceHeight - (UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 60)))
    
}

struct AppInfoFrames {
    static let bar: CGRect = CGRect(x:0,
                                    y:UIApplication.shared.statusBarFrame.height,
                                    width: ScreenInfo.currentDeviceWidth,
                                    height: ScreenInfo.getHeight(height: 60))
    static let back: CGRect = CGRect(x: ScreenInfo.getWidth(width:15),
                                     y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 15),
                                     width: ScreenInfo.getWidth(width: 28),
                                     height: ScreenInfo.getHeight(height: 28))
    static let titleLabel: CGRect = CGRect(x: ScreenInfo.getWidth(width:70),
                                           y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 10),
                                           width: ScreenInfo.getWidth(width: 300),
                                           height: ScreenInfo.getHeight(height: 37))
    
}

struct SignAlertFrames {
    static let txtField_leftView: CGRect = CGRect(x: 0,
                                                y: 0,
                                                width: ScreenInfo.getWidth(width: 10),
                                                height: ScreenInfo.getHeight(height: 40))
}
