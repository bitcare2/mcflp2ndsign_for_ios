//
//  Fonts.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 2..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation

struct LoginViewFonts {
    static let LogoTitle: UIFont = UIFont(name: "NanumGothicBold", size: ScreenInfo.getWidth(width:18))!
}

struct CertificationFonts {
    static let titleLabel: UIFont = UIFont(name: "NanumGothicBold", size: ScreenInfo.getHeight(height: 18))!            
    static let bgTitle: UIFont = UIFont(name: "NanumGothicBold", size: ScreenInfo.getWidth(width:20))!
    static let bgContent: UIFont = UIFont(name: "NanumGothicBold", size: ScreenInfo.getWidth(width:14))!
    static let confirm: UIFont = UIFont(name: "NanumGothicBold", size: ScreenInfo.getWidth(width:18))!
    static let supportTxt: UIFont = UIFont(name: "NanumGothic", size: ScreenInfo.getWidth(width:8))!
    static let LogoTitle: UIFont = UIFont(name: "NanumGothicBold", size: ScreenInfo.getWidth(width:18))!
}

struct LeftMenuFonts {
    static let name: UIFont = UIFont(name: "NanumGothic", size: ScreenInfo.getWidth(width:12))!
    static let hospital: UIFont = UIFont(name: "NanumGothic", size: ScreenInfo.getWidth(width:14))!
}

struct MainFonts {
    static let titleLabel: UIFont = UIFont(name: "NanumGothicBold", size: ScreenInfo.getHeight(height: 18))!
    static let rightMenu: UIFont =  UIFont(name: "NanumGothicBold", size: ScreenInfo.getHeight(height: 24))!
    
}

struct MainListCellFonts {
    static let name: UIFont = UIFont(name: "NanumGothicBold", size: ScreenInfo.getHeight(height: 16))!
    static let date: UIFont = UIFont(name: "NanumGothicBold", size: ScreenInfo.getHeight(height: 14))!
    static let type: UIFont = UIFont(name: "NanumGothicBold", size: ScreenInfo.getHeight(height: 18))!
    static let requestStateName: UIFont = UIFont(name: "NanumGothic", size: ScreenInfo.getHeight(height: 14))!
    static let state: UIFont = UIFont(name: "NanumGothicBold", size: ScreenInfo.getHeight(height: 14))!
    static let number: UIFont = UIFont(name: "NanumGothicBold", size: ScreenInfo.getHeight(height: 14))!
}

struct OZViewFonts {
    static let titleLabel: UIFont = UIFont(name: "NanumGothicBold", size: ScreenInfo.getHeight(height: 18))!
}

struct AppInfoFonts {
    static let titleLabel: UIFont = UIFont(name: "NanumGothicBold", size: ScreenInfo.getHeight(height: 18))!
}
