//
//  Strings.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 1..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation

struct LoginViewStrings {
    static let bottom: String = "국립연명의료관리기관"
    
}

struct CertificationStrings {
    static let navigationTitle: String = "연명의료 정보처리시스템"
    static let inquiry: String = "로그인이 안되신다면 정보처리시스템 기술지원 : 02-778-7567 으로 연락바랍니다."
    static let bottom: String = "국립연명의료관리기관"
}

struct CertificationPhase1Strings {
    static let title: String = "인증 번호 입력"
    static let content: String = "문자로 발송된 인증 번호를 입력해 주세요."
    static let placeholder: String = "인증 번호를 입력해 주세요."
    static let button: String = "인증번호 확인"
    
}

struct CertificationPhase2Strings {
    static let title: String = "인증 번호 입력"
    static let contentPhone: String = "휴대폰 번호를 입력해 주세요."
    static let contentDoc: String = "의사 번호를 입력해 주세요."
    static let contentCode: String = "전문의 번호를 입력해 주세요."
    static let placeholder: String = "숫자만 입력해 주세요."
    static let button: String = "인증 확인"
    
}

struct LeftMenuStrings {
    static let menuWait = "대기중인 서명목록"
    static let menuComplete = "완료한 서명목록"
    static let menuInfo = "정보"
    static let menuLogout = "로그아웃"
}

struct MainStrings {
    static let titleLabel1 = "대기중인 서명목록"
    static let titleLabel2 = "완료한 서명목록"
}

struct OZViewStrings {
    static let titleLabel = "대기중인 서명목"
}

struct AppInfoStrings {
    static let titleLabel = "정보"
}
