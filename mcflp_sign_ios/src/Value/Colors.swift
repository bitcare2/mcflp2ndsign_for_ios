//
//  Colors.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 2..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation

struct LoginViewColors {
    static let logoTitle: UIColor = HexColor.hexStringToUIColor(hex: "#FFFFFF")
}

struct CertificationColors {
    static let statusBarColor: UIColor = HexColor.hexStringToUIColor(hex: "#4380DB")
    static let titleLabel: UIColor = HexColor.hexStringToUIColor(hex: "#FFFFFF")
    static let bgTitle: UIColor = HexColor.hexStringToUIColor(hex: "#4380DB")
    static let bgContent: UIColor = HexColor.hexStringToUIColor(hex: "#1C1C1C")
    static let numberBorder: UIColor = HexColor.hexStringToUIColor(hex: "#9C9C9C")
    static let number: UIColor = HexColor.hexStringToUIColor(hex: "#ECECEC")
    static let supportTxt: UIColor = HexColor.hexStringToUIColor(hex: "#FFFFFF")
    static let logoTitle: UIColor = HexColor.hexStringToUIColor(hex: "#FFFFFF")
}

struct LeftMenuColors {
    static let bar: UIColor = HexColor.hexStringToUIColor(hex: "#2967C2")
    static let logoBG: UIColor = HexColor.hexStringToUIColor(hex: "#FFFFFF")
    static let name: UIColor = HexColor.hexStringToUIColor(hex: "#FFFFFF")
    static let hospital: UIColor = HexColor.hexStringToUIColor(hex: "#FFFFFF")
    static let manuWait: UIColor = HexColor.hexStringToUIColor(hex: "#000000")
    static let menuComplete: UIColor = HexColor.hexStringToUIColor(hex: "#000000")
    static let menuInfo: UIColor = HexColor.hexStringToUIColor(hex: "#000000")
    static let menuLogout: UIColor = HexColor.hexStringToUIColor(hex: "#000000")
}

struct MainColors {
    static let titleLabel: UIColor = HexColor.hexStringToUIColor(hex: "#FFFFFF")
    
}

struct MainListCellColors {
    static let name: UIColor = HexColor.hexStringToUIColor(hex: "#4283DB")
    static let date: UIColor = HexColor.hexStringToUIColor(hex: "#333333")
    static let type: UIColor = HexColor.hexStringToUIColor(hex: "#333333")
    static let requestStateName: UIColor = HexColor.hexStringToUIColor(hex: "#333333")
    static let state: UIColor = HexColor.hexStringToUIColor(hex: "#FFFFFF")
    static let number: UIColor = HexColor.hexStringToUIColor(hex: "#333333")
    
    
    static let stateBrown: UIColor = HexColor.hexStringToUIColor(hex: "#DC942D")
    static let stateWhine: UIColor = HexColor.hexStringToUIColor(hex: "#DB3583")
    static let stateGreen: UIColor = HexColor.hexStringToUIColor(hex: "#238c3c")
    static let stateBlue: UIColor = HexColor.hexStringToUIColor(hex: "#35A0DB")
}

struct OZViewColors {
    static let titleLabel: UIColor = HexColor.hexStringToUIColor(hex: "#FFFFFF")
    
}



struct AppInfoColors {
    static let titleLabel: UIColor = HexColor.hexStringToUIColor(hex: "#FFFFFF")
    
}
