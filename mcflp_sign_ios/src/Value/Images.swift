//
//  Images.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 2..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation

struct LoginViewImages {
    static let bgView: UIImageView = UIImageView(image: UIImage(named: "backgournd_splash.png"))
    static let logo: UIImageView = UIImageView(image: UIImage(named: "logo2.png"))
}

struct CertificationImages {
    static let bar: UIImageView = UIImageView(image: UIImage(named: "background_toolbar.png"))
    static let barlogo: UIImageView = UIImageView(image: UIImage(named: "logo2.png"))
    static let logo: UIImageView = UIImageView(image: UIImage(named: "logo2.png"))
    static let bgView: UIImageView = UIImageView(image: UIImage(named: "backgournd_splash.png"))
    static let confirm: UIImage = UIImage(named: "background_login_btn.png")!
}

struct LeftMenuImages {
    static let logo: UIImageView = UIImageView(image: UIImage(named: "logo2.png"))
}

struct MainImages {
    static let bar: UIImageView = UIImageView(image: UIImage(named: "background_toolbar.png"))
}

struct OZViewImages {
    static let bar: UIImageView = UIImageView(image: UIImage(named: "background_toolbar.png"))
}

struct AppInfoImages {
    static let bar: UIImageView = UIImageView(image: UIImage(named: "background_toolbar.png"))
    
}
