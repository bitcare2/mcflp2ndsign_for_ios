//
//  ApiDefine.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 18..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation


struct ApiDefine {
//    static let serverURl: String = "http://59.10.164.97:8080"
//    static let serverURl: String = "http://59.10.164.81:8080"
    static let serverURl: String = "http://nibp-mobile.g2e.co.kr"
    
    static let addDeviceInfo: String = "/sign/addDeviceInfo"
    static let checkLoginkey: String = "/sign/checkLoginkey"
    static let login: String = "/sign/login"
    static let autoLogin: String = "/sign/autoLogin"
    
    static let getFormList: String = "/sign/getFormList"
    static let getFormInfo: String = "/sign/getFormInfo"
    
    static let addSignImg: String = "/sign/addSignImg"
    static let addCancelSignImg: String = "/sign/addCancelSignImg"
}
