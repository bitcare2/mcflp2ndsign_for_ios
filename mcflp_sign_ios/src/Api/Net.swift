//
//  Net.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 18..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol NetProtocol {
    func getData()
}

class Net {
    
    init() {
        
    }
    
    //앱 최초 설치시 휴대폰 정보 저장
    public func getAddDeviceParams(deviceSID: String, deviceRegID: String, deviceType: String) -> Parameters {
        return [
            "deviceSID" : deviceSID,
            "deviceRegID" : deviceRegID,
            "deviceType" : deviceType
        ]
    }
    
    public func getCheckLoginKeyParams(deviceSID: String, deviceRegID: String, deviceType: String, loginType: String, loginKey: String) -> Parameters {
        return [
            "deviceSID" : deviceSID,
            "deviceRegID" : deviceRegID,
            "deviceType" : deviceType,
            "loginType" : loginType,
            "loginKey" : loginKey
        ]
    }
    
    public func getLoginParams(loginType: String, appDeviceSN: String, loginSN: String, deviceSID: String, deviceRegID: String, userSN: String, number: String) -> Parameters {
        return [
            "loginType" : loginType,
            "appDeviceSN" : appDeviceSN,
            "loginSN" : loginSN,
            "deviceSID" : deviceSID,
            "deviceRegID" : deviceRegID,
            "userSN" : userSN,
            "number" : number
        ]
    }
    
    public func getAutoLoginParams(token: String, userSN: String, deviceSID: String, deviceRegID: String)  -> Parameters {
        return [
            "token" : token,
            "userSN" : userSN,
            "deviceSID" : deviceSID,
            "deviceRegID" : deviceRegID
        ]
    }
    
    public func getFormListParams(token: String, startDate: String, endDate: String) -> Parameters {
        return [
            "token" : token,
            "startDate" : startDate,
            "endDate" : endDate
        ]
    }
    
    public func getFormInfoParams(token: String, signFormSN: String) -> Parameters {
        return [
            "token" : token,
            "signFormSN" : signFormSN
        ]
    }
    
    public func getAddSignImg(token: String, signFormSN: String) -> Parameters {
        return [
            "token": token,
            "signFormSN": signFormSN
        ]
    }
    
    public func addCancelSignImg(token: String, signFormSN: String) -> Parameters {
        return [
            "token": token,
            "signFormSN": signFormSN
        ]
    }
    
    public func request(url: String, params: Parameters, data: @escaping(_ deta: JSON) -> Void) {
        print("request url: \(url) params: \(params)")
        Alamofire.request(ApiDefine.serverURl + url,
                          method: .post,
                          parameters: params,
                          encoding: URLEncoding.default,
                          headers: ["Content-Type":"application/x-www-form-urlencoded", "Accept":"application/json;charset=UTF-8"])
            .validate(statusCode:200..<300)
            .responseJSON{
                (response) -> Void in
                
                print("response.result.isSuccess: \(response.result.isSuccess)")
                
                guard response.result.isSuccess else {
                    return
                }
                
                do {
                    data(try JSON(data: response.data!))
                } catch {
                    
                }
        }
    }
    
    public func uploadPNG(url: String, img: UIView, params: Parameters, data: @escaping(_ deta: JSON) -> Void    ) {
        
        UIGraphicsBeginImageContextWithOptions(img.bounds.size, false, UIScreen.main.scale)
        img.drawHierarchy(in: img.bounds, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let imgData = UIImagePNGRepresentation(image!)!
        
        let apiUrl: URL = URL(string: ApiDefine.serverURl + url)!
        let headers = [
            "Content-Type": "multipart/form-data" //application/json, multipart/form-data
        ]
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "singImg ",fileName: "file1.png", mimeType: "image/png")
            
            for (key, value) in params {
                print("key: \(key), value: \(value)")
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
            
        }, to:apiUrl, method: .post, headers: headers) { (result) in
            switch result {
            case .success(let upload, _, _) :
                print("success")
                upload.responseJSON { (response) -> Void in
                    
                    guard response.result.isSuccess else {
                        return
                    }
                    
                    do {
                        data(try JSON(data: response.data!))
                    } catch {
                        
                    }
                    
                }
            case .failure(_):
                print("failure")
                
            }
        }
    }
}
