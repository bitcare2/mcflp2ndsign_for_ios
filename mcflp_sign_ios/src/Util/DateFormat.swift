//
//  DateFormatter.swift
//  mcflp_sign_ios
//
//  Created by LIMSUNGHYUN on 06/12/2018.
//  Copyright © 2018 bitcare. All rights reserved.
//

import Foundation

class DateFormat {
    
    public static func toDate(format: String, date: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date: Date = dateFormatter.date(from: date)!
        
        return date
    }
    
    public static func toString(before: String, after: String, date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = before
        let beforeDate: Date = dateFormatter.date(from: date)!
        
        dateFormatter.dateFormat = after
        let afterStrDate: String = dateFormatter.string(from: beforeDate)
        
        
        return afterStrDate
    }
}
