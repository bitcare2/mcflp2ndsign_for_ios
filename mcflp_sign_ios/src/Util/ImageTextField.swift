//
//  ImageTextField.swift
//  mcflpsign
//
//  Created by seok on 2018. 7. 11..
//  Copyright © 2018년 com.bit.bitcare. All rights reserved.
//

import UIKit
import Foundation

class ImageTextField: UITextField {
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateLeftView()
        }
    }
    
    @IBInspectable var padding : CGFloat = 20 {
        didSet {
            updateLeftView()
        }
    }
    
//    @IBInspectable var cornerRadius : CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = cornerRadius
//        }
//    }
    
    @IBInspectable var placeholderColour : UIColor = UIColor.white {
        didSet {
            attributedPlaceholder = NSAttributedString(string: placeholder != nil ? placeholder! : "", attributes: [:])
        }
    }
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateRightView()
        }
    }
    
//    override func textRect(forBounds bounds: CGRect) -> CGRect {
//        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, 65, 0, 15))
//    }
//
//    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
//        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, 65, 0, 15))
//    }
    
    func updateRightView() {
        if let image = rightImage {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = image
            
            let width = 20 + padding
            
            imageView.tintColor = placeholderColour
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            
            rightViewMode = .always
            rightView = view;
        } else {
            rightView = nil
            rightViewMode = .never
        }
    }
    
    func updateLeftView() {
        if let image = leftImage {
            let imageView = UIImageView(frame: CGRect(x: padding, y: 0, width: 20, height: 20))
            imageView.image = image
            
            var width = 20 + padding
            
            if borderStyle == .none || borderStyle == .line {
                width += 5
            }
            
            imageView.tintColor = placeholderColour
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            
            leftViewMode = .always
            leftView = view
            
        } else {
            leftView = nil
            rightViewMode = .never
        }
    }
}
