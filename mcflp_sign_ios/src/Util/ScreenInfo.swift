//
//  ScreenInfo.swift
//  mcflp_tablet_ios
//
//  Created by seok on 2018. 9. 27..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation
import UIKit

class ScreenInfo {
    
    public static let standardDeviceWidth = CGFloat(375.0)
    public static let standardDeviceHeight = CGFloat(667.0)
    public static let currentDeviceWidth = UIScreen.main.bounds.width
    public static let currentDeviceHeight = UIScreen.main.bounds.height
    public static let ratioWidth = currentDeviceWidth / standardDeviceWidth
    public static let ratioHeight = currentDeviceHeight / standardDeviceHeight
    
    public static func getWidth(width: CGFloat) -> CGFloat {
        return width * ratioWidth
    }
    
    public static func getHeight(height: CGFloat) -> CGFloat {
        return height * ratioHeight
    }
}
