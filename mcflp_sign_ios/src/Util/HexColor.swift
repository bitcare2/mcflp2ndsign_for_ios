//
//  UIColor.swift
//  testButton1
//
//  Created by seok on 2018. 6. 19..
//  Copyright © 2018년 seok. All rights reserved.
//

import Foundation
import UIKit

class HexColor {
    
    public static func hexStringToUIColor(hex: String) -> UIColor {
        var hexStr: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if(hexStr.hasPrefix("#")) {
            hexStr.remove(at: hexStr.startIndex)
        }
        
        if((hexStr.characters.count) != 6) {
            return UIColor.black
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: hexStr).scanHexInt32(&rgbValue)
        
        return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                       green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                       blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                       alpha: CGFloat(1.0)
        )
    }
}
