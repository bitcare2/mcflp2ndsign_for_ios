//
//  UIFrame.swift
//  mcflp_sign_ios
//
//  Created by seok on 2018. 10. 2..
//  Copyright © 2018년 bitcare. All rights reserved.
//

import Foundation

struct CertificationFrames {
    static let titleLogo: CGRect = CGRect(x: ScreenInfo.getWidth(width:15),
                                          y:UIApplication.shared.statusBarFrame.height + ScreenInfo.getHeight(height: 15),
                                          width: ScreenInfo.getWidth(width: 28),
                                          height: ScreenInfo.getHeight(height: 28))
    
}
