//
//  OZReportViewer.h
//  ozviewer51_ios
//
//  Created by econquer on 10. 10. 25..
//  Copyright 2010 FORCS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol OZReportCommandListener;


@class OZRViewerCmd;

@interface OZReportViewer : NSObject {

}

-(UIView*) view;
-(UIViewController*) viewController;
-(void) CreateReport:(NSString*)str_param;
-(void) CreateReportEx:(NSString*)str_param;
-(void) CreateReportEx:(NSString*)str_param delimiter:(NSString*) var_delimiter;
-(void) ReBind:(int)nIndex type:(NSString*)str_type param:(NSString*)str_param;
-(void) ReBind:(int)nIndex type:(NSString*)str_type param:(NSString*)str_param delimiter:(NSString*)var_delimiter;
-(void) ReBind:(int)nIndex type:(NSString*)str_type param:(NSString*)str_param delimiter:(NSString*)var_delimiter keepEditing:(BOOL)keepEditing;

-(void) NewReport:(NSString*)str_param delimiter:(NSString*) var_delimiter;
-(void) setListener:(id<OZReportCommandListener>)cmdListener;


//-(void) init: (OZEmbededListener*) listener;	
-(NSString*) GetInformation: (NSString*) str_item;
-(void) Script:(NSString*) str;	
-(int) ScriptEx:(NSString*) str_cmd param:(NSString*)str_param delimiter:(NSString*) str_delimiter;
-(OZRViewerCmd*) Document;
-(void) setHelpURL:(NSString*)url;

-(NSDictionary*) GetMemoryStreamByExport:(NSString*)str_param delimiter:(NSString*) str_delimiter;

-(void)preLoadRepositoryInitParam:(NSString*)str_param delimiter:(NSString*) str_delimiter;
-(void)preLoadRepositoryAddItem:(NSString*)strCategory itemName:(NSString*) strItemName;
-(void)preLoadRepositoryAddBinary:(NSString*)str_key inputStream:(NSInputStream*)inputStream isOZD:(BOOL)isOZD;
-(BOOL)preLoadRepositoryDownload;
-(NSString*)preLoadRepositoryGetLastError;

-(BOOL) Document_PingOZServer:(NSString*)url;
-(BOOL) Document_PingOZServer:(NSString*)url port:(int) port;

-(NSString*) Document_GetGlobal:(NSString*)key;
-(NSString*) Document_GetGlobal:(NSString*)key docIndex:(int)varDocIndex;

-(BOOL) Document_SetGlobal:(NSString*)key asBoolean:(BOOL)value;
-(BOOL) Document_SetGlobal:(NSString*)key asInteger:(int)value;
-(BOOL) Document_SetGlobal:(NSString*)key asDouble:(double)value;
-(BOOL) Document_SetGlobal:(NSString*)key asString:(NSString*)value;

-(BOOL) Document_SetGlobal:(NSString*)key asBoolean:(BOOL)value docIndex:(int)varDocIndex;
-(BOOL) Document_SetGlobal:(NSString*)key asInteger:(int)value docIndex:(int)varDocIndex;
-(BOOL) Document_SetGlobal:(NSString*)key asDouble:(double)value docIndex:(int)varDocIndex;
-(BOOL) Document_SetGlobal:(NSString*)key asString:(NSString*)value docIndex:(int)varDocIndex;

-(BOOL) Document_SetChartStyle:(NSString*)style;
-(NSString*) Document_GetTitle;
-(float) Document_GetPaperWidth;
-(float) Document_GetPaperHeight;

-(NSString*) Document_TriggerExternalEvent;
-(NSString*) Document_TriggerExternalEvent:(NSString*)param1;
-(NSString*) Document_TriggerExternalEvent:(NSString*)param1 param2:(NSString*)param2;
-(NSString*) Document_TriggerExternalEvent:(NSString*)param1 param2:(NSString*)param2 param3:(NSString*)param3;
-(NSString*) Document_TriggerExternalEvent:(NSString*)param1 param2:(NSString*)param2 param3:(NSString*)param3 param4:(NSString*)param4;

-(NSString*) Document_TriggerExternalEventByDocIndex:(int)docindex;
-(NSString*) Document_TriggerExternalEventByDocIndex:(int)docindex param1:(NSString*)param1;
-(NSString*) Document_TriggerExternalEventByDocIndex:(int)docindex param1:(NSString*)param1 param2:(NSString*)param2;
-(NSString*) Document_TriggerExternalEventByDocIndex:(int)docindex param1:(NSString*)param1 param2:(NSString*)param2 param3:(NSString*)param3;
-(NSString*) Document_TriggerExternalEventByDocIndex:(int)docindex param1:(NSString*)param1 param2:(NSString*)param2 param3:(NSString*)param3 param4:(NSString*)param4;

-(NSString*) Document_TriggerLocationUpdated:(NSString*)locationInfo addressInfo:(NSString*)addressInfo;
-(NSString*) Document_TriggerLocationUpdatedByDocIndex:(int)docIndex locationInfo:(NSString*)locationInfo addressInfo:(NSString*)addressInfo;

@end
