//
//  OZReportAPI.h
//  ozviewer51_ios
//
//  Created by econquer on 10. 10. 27..
//  Copyright 2010 FORCS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OZReportViewer.h"
#import "OZRViewerCmd.h"

#define OZPACK_ENTRY __attribute__((visibility("default")))

#ifdef __cplusplus
extern "C" {
#endif
    
OZPACK_ENTRY void __OZPack_load_recognition();
OZPACK_ENTRY void __OZPack_load_ozc_pdf();
OZPACK_ENTRY void __OZPack_load_stylus();
OZPACK_ENTRY void __OZPack_load_address();
OZPACK_ENTRY void __OZExportPack_load_excel();
OZPACK_ENTRY void __OZExportPack_load_excel2003();
OZPACK_ENTRY void __OZExportPack_load_ppt();
OZPACK_ENTRY void __OZExportPack_load_pdf();
OZPACK_ENTRY void __OZExportPack_load_hwp();
OZPACK_ENTRY void __OZPack_load_OZUSLClient();

    
#ifdef __cplusplus
    }
#endif

@protocol OZReportCommandListener;

@interface OZReportAPI : NSObject {
    
}

// Deprecated Method.. ================================================================================================================================================================
+(OZReportViewer*) viewerWithParam: (UIViewController*)controller view:(UIView*)parent param:(NSString*)paramStr DEPRECATED_ATTRIBUTE;
+(OZReportViewer*) viewerWithParam2: (UIViewController*)controller view:(UIView*)parent listener:(id<OZReportCommandListener>)cmdListener param:(NSString*)paramStr closeButton:(NSString*)buttonText DEPRECATED_ATTRIBUTE;

+(OZReportViewer*) viewerWithParamEx: (UIViewController*)controller view:(UIView*)parent param:(NSString*)paramStr delimiter:(NSString*)delimiter DEPRECATED_ATTRIBUTE;
+(OZReportViewer*) viewerWithParamEx2: (UIViewController*)controller view:(UIView*)parent listener:(id<OZReportCommandListener>)cmdListener param:(NSString*)paramStr delimiter:(NSString*)delimiter closeButton:(NSString*)buttonText DEPRECATED_ATTRIBUTE;

+(OZReportViewer*) viewerWithParam_NotStart: (UIViewController*)controller view:(UIView*)parent DEPRECATED_ATTRIBUTE;
+(OZReportViewer*) viewerWithParam_NotStart2: (UIViewController*)controller view:(UIView*)parent closeButton:(NSString*)buttonText DEPRECATED_ATTRIBUTE;
// ====================================================================================================================================================================================


+(OZReportViewer*) createViewer: (UIViewController*)controller view:(UIView*)parent param:(NSString*)paramStr;
+(OZReportViewer*) createViewer: (UIViewController*)controller view:(UIView*)parent listener:(id<OZReportCommandListener>)cmdListener param:(NSString*)paramStr closeButton:(NSString*)buttonText;

+(OZReportViewer*) createViewer: (UIViewController*)controller view:(UIView*)parent param:(NSString*)paramStr delimiter:(NSString*)delimiter;
+(OZReportViewer*) createViewer: (UIViewController*)controller view:(UIView*)parent listener:(id<OZReportCommandListener>)cmdListener param:(NSString*)paramStr delimiter:(NSString*)delimiter closeButton:(NSString*)buttonText;

+(UIView*)createComponent:(UIView*)parent compType:(NSString*)compType option:(NSDictionary*)option;

+(float)contentScaleFactorMax;
+(void)setContentScaleFactorMax:(float)limit;

+(void*)imageSizeFilter;
+(void)setImageSizeFilter:(void*)value;

+(BOOL)isDebugHTTP;
+(void)setDebugHTTP:(BOOL)value;

+(float)requestTimeout;
+(void)setRequestTimeout:(float)seconds;

+(void)setCustomOption:(NSString*)value forKey:(NSString*)key;
+(NSString *)customOptionForKey:(NSString*)key;

@end


@protocol OZReportCommandListener <NSObject>

@optional
- (void) OZCloseCommand;

- (void) OZPostCommand: (NSString*)cmd :(NSString*)msg;
- (void) OZPrintCommand: (NSString*)msg :(NSString*)code :(NSString*)reportname :(NSString*)printername :(NSString*)printcopy :(NSString*)printedpage :(NSString*)printrange :(NSString*)username :(NSString*)drivername :(NSString*)printpagesrange;
- (void) OZExportCommand: (NSString*)code :(NSString*)path :(NSString*)filename :(NSString*)pagecount :(NSString*)filepaths;
- (void) OZProgressCommand: (NSString*)step :(NSString*)state :(NSString*)reportname;
- (void) OZErrorCommand: (NSString*)code :(NSString*)message :(NSString*)detailmessage :(NSString*)reportname;
- (void) OZCommand: (NSString*)code :(NSString*)args;
- (void) OZExitCommand;
- (void) OZMailCommand: (NSString*)code;
- (void) OZUserActionCommand: (NSString*)type :(NSString*)attr;
- (void) OZLinkCommand: (NSString*)docindex :(NSString*)componentname :(NSString*)usertag :(NSString*)uservalue :(NSString*)mousebutton;
- (void) OZBankBookPrintCommand: (NSString*)datas;
- (void) OZPageChangeCommand: (NSString*)docindex;
- (void) OZPageBindCommand: (NSString*)docindex :(NSString*)pagecount;
- (void) OZReportChangeCommand: (NSString*)docindex;
- (void) OZEFormInputEventCommand: (NSString*)docindex :(NSString*)formid :(NSString*)eventname :(NSString*)mainscreen;
- (void) OZExportMemoryStreamCallBack: (NSString*)outputdata;
- (void) OZEFormCustomEvent:(NSString*)info;
    
@optional
- (NSString*) OZUserEvent: (NSString*)param1 :(NSString*)param2 :(NSString*)param3;
- (BOOL) OZWillChangeIndex_Paging: (int)newIndex :(int)oldIndex;

@optional
- (void)OZTextBoxCommand:(OZRTextBoxCmd*)textBoxCmd mode:(int)mode; // mode: 0=OPEN, 1=CLOSE(OK), -1=CLOSE(CANCEL)

@end
