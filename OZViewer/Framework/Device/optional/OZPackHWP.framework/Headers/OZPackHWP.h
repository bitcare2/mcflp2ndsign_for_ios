//
//  OZPackPDF.h
//  OZPackPDF
//
//  Created by 이종현 on 2015. 6. 18..
//  Copyright (c) 2015년 FORCS. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for OZPackPDF.
FOUNDATION_EXPORT double OZPackHWPVersionNumber;

//! Project version string for OZPackPDF.
FOUNDATION_EXPORT const unsigned char OZPackHWPVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OZPackPDF/PublicHeader.h>


#import <OZPackHWP/OZPackHWP.h>

#define OZPACK_ENTRY __attribute__((visibility("default")))

#ifdef __cplusplus
extern "C" {
#endif
    
    OZPACK_ENTRY void __OZExportPack_load_hwp();
#ifdef __cplusplus
}
#endif
