//
//  OZPackRecognition.h
//  OZPackRecognition
//
//  Created by 이종현 on 2015. 6. 18..
//  Copyright (c) 2015년 FORCS. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for OZPackRecognition.
FOUNDATION_EXPORT double OZPackRecognitionVersionNumber;

//! Project version string for OZPackRecognition.
FOUNDATION_EXPORT const unsigned char OZPackRecognitionVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OZPackRecognition/PublicHeader.h>


#import <OZPackRecognition/OZPackRecognition.h>

#define OZPACK_ENTRY __attribute__((visibility("default")))

#ifdef __cplusplus
extern "C" {
#endif
    
    OZPACK_ENTRY void __OZPack_load_recognition();
#ifdef __cplusplus
}
#endif