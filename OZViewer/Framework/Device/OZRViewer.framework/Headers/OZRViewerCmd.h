//
//  OZRViewerCmd.h
//  ozviewer51_ios
//
//  Created by econquer on 10. 11. 5..
//  Copyright 2010 FORCS. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OZRViewerCmd : NSObject {
}

-(BOOL) PingOZServer:(NSString*)url;
-(BOOL) PingOZServer:(NSString*)url port:(int) port;

-(NSString*) GetGlobal:(NSString*)key;
-(NSString*) GetGlobal:(NSString*)key docIndex:(int)varDocIndex;

-(BOOL) SetGlobal:(NSString*)key asBoolean:(BOOL)value;
-(BOOL) SetGlobal:(NSString*)key asInteger:(int)value;
-(BOOL) SetGlobal:(NSString*)key asDouble:(double)value;
-(BOOL) SetGlobal:(NSString*)key asString:(NSString*)value;

-(BOOL) SetGlobal:(NSString*)key asBoolean:(BOOL)value docIndex:(int)varDocIndex;
-(BOOL) SetGlobal:(NSString*)key asInteger:(int)value docIndex:(int)varDocIndex;
-(BOOL) SetGlobal:(NSString*)key asDouble:(double)value docIndex:(int)varDocIndex;
-(BOOL) SetGlobal:(NSString*)key asString:(NSString*)value docIndex:(int)varDocIndex;

-(BOOL) SetChartStyle:(NSString*)style;
-(NSString*) GetTitle;
-(float) GetPaperWidth;
-(float) GetPaperHeight;

-(NSString*) TriggerExternalEvent;
-(NSString*) TriggerExternalEvent:(NSString*)param1;
-(NSString*) TriggerExternalEvent:(NSString*)param1 param2:(NSString*)param2;
-(NSString*) TriggerExternalEvent:(NSString*)param1 param2:(NSString*)param2 param3:(NSString*)param3;
-(NSString*) TriggerExternalEvent:(NSString*)param1 param2:(NSString*)param2 param3:(NSString*)param3 param4:(NSString*)param4;

-(NSString*) TriggerExternalEventByDocIndex:(int)docindex;
-(NSString*) TriggerExternalEventByDocIndex:(int)docindex param1:(NSString*)param1;
-(NSString*) TriggerExternalEventByDocIndex:(int)docindex param1:(NSString*)param1 param2:(NSString*)param2;
-(NSString*) TriggerExternalEventByDocIndex:(int)docindex param1:(NSString*)param1 param2:(NSString*)param2 param3:(NSString*)param3;
-(NSString*) TriggerExternalEventByDocIndex:(int)docindex param1:(NSString*)param1 param2:(NSString*)param2 param3:(NSString*)param3 param4:(NSString*)param4;

-(NSString*) TriggerLocationUpdated:(NSString*)locationInfo addressInfo:(NSString*)addressInfo;
-(NSString*) TriggerLocationUpdatedByDocIndex:(int)docIndex locationInfo:(NSString*)locationInfo addressInfo:(NSString*)addressInfo;
@end



@interface OZRTextBoxCmd : NSObject
@property (nonatomic, readonly) NSString* componentName;

-(NSString*)text;
-(void)setText:(NSString*)text;

-(NSString*)parameter;
-(void)setParameter:(NSString*)param;

-(void)endEdit;
@end

