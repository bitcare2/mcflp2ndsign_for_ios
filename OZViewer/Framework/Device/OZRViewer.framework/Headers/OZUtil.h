//
//  OZUtil.h
//  ozviewer60_ios
//
//  Created by kooltz on 2015. 1. 9..
//  Copyright (c) 2015년 FORCS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OZUtil : NSObject

+ (NSData *)monoBitmapConvertor:(UIImage *)srcImage scaleWidth:(int)scaleWidth scaleHeight:(int)scaleHeight;

+ (NSData *)decodeBase64String:(NSString *)aString;
+ (NSString *)encodeBase64String:(NSData *)aData;

@end
