//
// Do not modify this file!
//

//
//  OZUserSignPad_iPad_H.h
//
//  Created by econquer on 12. 1. 10..
//  Copyright (c) 2012년 FORCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OZUserSignPad_iPad_H : UIViewController
{
    UIButton* m_btnDraw;
    UIButton* m_btnErase;
    BOOL    m_bEraseMode;
}

@property (assign, nonatomic) UIView* ozConnector;
@property (retain, nonatomic) IBOutlet UIView* signPad;
@property (retain, nonatomic) IBOutlet UIImageView* topView;
@property (retain, nonatomic) IBOutlet UIImageView* mainView;
@property (retain, nonatomic) IBOutlet UIView* backgroundView;
    
@property (retain, nonatomic) IBOutlet UIButton* btnConfirm;
@property (retain, nonatomic) IBOutlet UIButton* btnInit;

-(IBAction) onOK;
-(IBAction) onReset;

@end
